package mcs60.client;

import java.util.UUID;

/**
 * Created by dare on 7/26/2015.
 */
public class TransferRowDetail {

    private UUID transferID;
    private int totalPieces;
    private int piecesDone = 0;
    private boolean finished = false;
    private boolean paused = false;
    private String transferType;

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public String getTransferType() {
        return transferType;
    }

    public TransferRowDetail(UUID transferID, int totalPieces, String type) {
        this.transferID = transferID;
        this.totalPieces = totalPieces;
        this.transferType = type;
    }

    public UUID getTransferID() {
        return transferID;
    }

    public int getTotalPieces() {
        return totalPieces;
    }

    public int getPiecesDone() {
        return piecesDone;
    }

    public void setPiecesDone(int piecesDone) {
        this.piecesDone = piecesDone;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
