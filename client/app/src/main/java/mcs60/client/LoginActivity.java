package mcs60.client;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends Activity {

    private static final String TAG= "LoginActivity";
    private static final int REQUEST_GATEWAY_DETAILS = 458;


    private EditText txtUserName;
    private EditText txtPwd;

    private String srvAddress;
    private String srvPort;

    private CoreService core;
    private boolean isBound = false;
    private boolean isAuthorized = false;


    // for shared preferences
    private final String APP_NAME = "mcs60.client";
    private SharedPreferences settings ;
    private SharedPreferences.Editor settingsEditor;
    // key names
    private final String setting_srvAddress = "srvAddress";
    private final String setting_srvPort = "srvPort";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // get reference to edit texts
        txtUserName = (EditText) findViewById(R.id.loginEdtUsrName);
        txtPwd = (EditText) findViewById(R.id.loginEdtPwd);

        // get preferences
        settings = PreferenceManager.getDefaultSharedPreferences(this);

        validateServerParameters();

        Intent inSrv = new Intent(this, CoreService.class);
        bindService(inSrv,coreConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isAuthorized)
            gotoMainActivity();
    }

    @Override
    protected void onDestroy() {
        unbindService(coreConnection);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    Retrieve server details from preferences
    If not found, redirect to GatewayActivity
     */
    private void validateServerParameters() {
        setServerParams(settings.getString(setting_srvAddress, ""),
                settings.getString(setting_srvPort, ""));

        if(srvAddress.equals("") || srvPort.equals("")) {
            gotoGatewayActivity(null);
        }
    }

    private void setServerParams(String addr, String port) {
        this.srvAddress = addr;
        this.srvPort = port;
    }

    /*
    Goto GatewayActivity
     */
    public void gotoGatewayActivity(View view) {
        Intent i = new Intent(this,GatewayActivity.class);
        i.putExtra("srvAddr", srvAddress);
        i.putExtra("srvPort", srvPort);
        startActivityForResult(i, REQUEST_GATEWAY_DETAILS);
    }

    /*
    Goto MainActivity
     */
    public void gotoMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // result was from GatewayActivity
        if(requestCode == REQUEST_GATEWAY_DETAILS) {
            if (resultCode == RESULT_OK) {
                setServerParams(data.getStringExtra("resultAddr"), data.getStringExtra("resultPort"));
                settingsEditor = settings.edit();
                settingsEditor.putString(setting_srvAddress, srvAddress);
                settingsEditor.putString(setting_srvPort, srvPort);
                settingsEditor.commit();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    //CoreService Connection
    private ServiceConnection coreConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            CoreService.CoreBinder binder = (CoreService.CoreBinder) iBinder;
            core = binder.getService();
            isBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBound = false;
        }
    };

    public void login(View view) {
        String username = txtUserName.getText().toString();
        String pwd = txtPwd.getText().toString();
        boolean authorized;

        if (username.equals("") || pwd.equals(""))
            return;


        core.setServerInfo(srvAddress, srvPort);
        isAuthorized = core.authorize(username, pwd);
        if (isAuthorized) {
            Toast.makeText(getApplicationContext(), "Logged in", Toast.LENGTH_SHORT).show();
            gotoMainActivity();
        }
        else
            Toast.makeText(getApplicationContext(), "Invalid details",Toast.LENGTH_SHORT).show();
    }
}
