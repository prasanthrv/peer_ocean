package mcs60.client;


import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.UUID;

import mcs60.common.bundle.Piece;
import mcs60.common.protocol.*;
import mcs60.common.protocol.messages.*;
import mcs60.common.utility.Util;

/**
 * Created by dare on 30/06/15.
 */
public class ServerConnection {

    private final String TAG;

    private Socket socket;
    private ObjectOutputStream outObjStream;
    private ObjectInputStream inObjStream;
    private InputStream inStream;
    private OutputStream outStream;

    private BufferedOutputStream bufOut;
    private BufferedInputStream bufIn;
    private final int BUFFER_SIZE = Util.BUFFER_SIZE;

    private String srvAddr;
    private int srvPort;
    private UUID clientID;
    private MessageFrame msg;


    public ServerConnection(String address, int port, String TAG, UUID clientID) {

        this.TAG = TAG;
        this.clientID = clientID;
        srvAddr = address;
        srvPort = port;
    }

    /*
    Connect to server, open streams
    Return false if connect fails
     */
    public boolean connect() {
        try {
            SocketAddress sockaddr = new InetSocketAddress(srvAddr, srvPort);
            socket = new Socket();
            socket.connect(sockaddr, 2000);
            socket.setSoTimeout(1000);

            //bufIn = new BufferedInputStream(socket.getInputStream());
            //bufOut = new BufferedOutputStream(socket.getOutputStream());
            //outObjStr = new ObjectOutputStream(bufOut);
            //in = new ObjectInputStream(bufIn);

            inStream = socket.getInputStream();
            outStream = socket.getOutputStream();

            outObjStream = new ObjectOutputStream(outStream);
            inObjStream = new ObjectInputStream(inStream);

            //outObjStr = new ObjectOutputStream(socket.getOutputStream());
            //inObjStr = new ObjectInputStream(socket.getInputStream());

            Log.i(TAG, "Connection to " + srvAddr + ":" + srvPort);

            return true;
        } catch (Exception e) {
            Log.i(TAG, "Connection to " + srvAddr + ":" + srvPort + " failed");
            Log.e(TAG, e.toString());
            return false;
        }
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            Log.e(TAG, "Socket could not be closed");
        }
    }

    /* Is the connection still valid ? */
    public boolean isValid() {
        if (socket == null)
            return  false;
        return !socket.isClosed();
    }

    /* Send authentication message */
    public UUID sendAuthMessage(String username, String pwd) {

        return sendMessage( new AuthMessage(Util.hashSHA256(username.getBytes()),
                Util.hashSHA256(pwd.getBytes()), clientID));
    }

    /* Send a piece with data */
    public UUID sendPieceContent(Piece piece, byte[] data) throws Exception {

        return sendMessage(new PieceContentMessage(piece, data, clientID));
    }

    /* Send a piece request */
    public UUID sendPieceRequest(Piece piece) throws Exception {

        return sendMessage(new PieceRequestMessage(piece, clientID));
    }

    /* receive data as bytes from server */
    public boolean receivePiece(int pieceLength, String path) {

        //pieceLength -= 1;
        Log.i(TAG, "Given piece length: " + pieceLength);
        byte[] buffer = new byte[BUFFER_SIZE];
        File piece = new File(path);
        int bytesRead = 0;
        try {
            //bufIn = new BufferedInputStream(socket.getInputStream());
            //InputStream bufIn = socket.getInputStream();
            BufferedOutputStream fileOut = new BufferedOutputStream(new FileOutputStream(piece));


            while(bytesRead!= pieceLength && inStream.read(buffer) != -1) {

                bytesRead += buffer.length;
                fileOut.write(buffer);
                fileOut.flush();

                int remainingSize = pieceLength - bytesRead;
                if (remainingSize == 0)
                    break;
                else if ( remainingSize > 0 && remainingSize < BUFFER_SIZE) {
                    buffer = new byte[remainingSize];
                }
                /*if (bytesRead >= pieceLength) {
                    break;
                }
                */
            }

            fileOut.close();
            Log.i(TAG, "File size:" + piece.length());
            Log.i(TAG, "Written piece length: " + bytesRead);

        } catch (IOException e) {
            Log.i(TAG, "Error in receiving piece");
            e.printStackTrace();
            return false;
        }


        Log.i(TAG, "Received piece: " + piece.getName());
        return true;
    }
    /* get piece data transferred by the server */
    public byte[] receivePieceData() throws Exception{

        MessageFrame msg = receiveMessage();
        if (msg!= null && msg instanceof PieceContentMessage)
            return ((PieceContentMessage) msg).getData();
        else
            return null;


    }

    /* the client received ACK to the previous message */
    public boolean receivedACK(UUID prevMsgID) {

        MessageFrame msg = receiveMessage();
        if (msg!= null && msg.getRequestType() == MessageFrame.MESSAGE_TYPE.ACK &&
                prevMsgID.equals(msg.getMsgID()))
            return true;
        else
            return false;

    }

    /* receive InfoMessage */
    public String receiveInfo() {

        MessageFrame msg = receiveMessage();
        if (msg!= null && msg instanceof InfoMessage)
            return ((InfoMessage) msg).getInfo();
        else
            return null;

    }

    /* receive PieceLengthMessage */
    public int receivePieceLength() {

        MessageFrame msg = receiveMessage();
        if (msg!= null && msg instanceof PieceLengthMessage)
            return ((PieceLengthMessage) msg).getLength();
        else
            return 0;

    }

    /* get a message from the server */
    public MessageFrame receiveMessage() {
        MessageFrame msg = null;
        while(socket.isConnected()) {
            try {
                //in.reset();
                //in = new ObjectInputStream(socket.getInputStream());
                msg = (MessageFrame) inObjStream.readObject();
                return msg;
            } catch (SocketTimeoutException sc) {
                Log.e(TAG, "Socket timed out");
                connect();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "IO error in receiving message");
                //connect();
                break;
            } catch (ClassCastException e) {
                Log.e(TAG, "Cannot cast message");
            } catch (ClassNotFoundException e) {
                Log.e(TAG, "Cannot find class");
            }
        }
            return msg;

    }

    /* send a message to the server */
    public UUID sendMessage(MessageFrame msg) {
        while(socket.isConnected()) {
            try {
                UUID curMsgID = msg.getMsgID();
                //outObjStr.reset();
                outObjStream.writeObject(msg);
                outObjStream.flush();
                return  curMsgID;
            } catch (SocketTimeoutException sc) {
                Log.e(TAG, "Socket timed out");
                connect();
            } catch (IOException e) {
                Log.e(TAG, "IO error in sending");
            } catch (ClassCastException e) {
                Log.e(TAG, "Cannot cast message");
            }
        }
            return null;
    }
}
