package mcs60.client;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by dare on 7/26/2015.
 */
public class TransferAdapter  extends ArrayAdapter<TransferRowDetail> {

    TransferControl transferControl;

    public TransferAdapter(Context context, int resource, List<TransferRowDetail> objects, TransferControl transferControl) {
        super(context, R.layout.transfer_row, objects);
        this.transferControl = transferControl;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());

        final View row = inflater.inflate(R.layout.transfer_row, parent, false);

        final TransferRowDetail transfer = getItem(position);

        TextView txtTransferType = (TextView) row.findViewById(R.id.txtTransferItemType);
        TextView txtTransferStatus = (TextView) row.findViewById(R.id.txtTransferItemStatus);
        TextView txtTransferName = (TextView) row.findViewById(R.id.txtTransferItemName);
        final ImageButton btnTransferRun = (ImageButton) row.findViewById(R.id.btnTransferItemRun);
        ImageButton btnTransferExit = (ImageButton) row.findViewById(R.id.btnTransferItemExit);
        ProgressBar pgrTransfer = (ProgressBar) row.findViewById(R.id.pgrTransferItem);

        pgrTransfer.setMax(transfer.getTotalPieces());
        pgrTransfer.setProgress(transfer.getPiecesDone());

        txtTransferType.setText(transfer.getTransferType());
        txtTransferName.setText("Transfer: #" + position + 1);

        if (transfer.isFinished()){
            btnTransferRun.setVisibility(View.INVISIBLE);
            txtTransferStatus.setText("Completed");
        } else if(transfer.isPaused()) {
            btnTransferRun.setBackground(row.getResources().getDrawable(R.drawable.play));
            txtTransferStatus.setText("Paused");
        } else {
            btnTransferRun.setBackground(row.getResources().getDrawable(R.drawable.pause));
            txtTransferStatus.setText("Running");
        }

        btnTransferRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (transfer.isPaused()) {
                    transfer.setPaused(false);
                    btnTransferRun.setBackground(row.getResources().getDrawable(R.drawable.pause));
                } else {
                    transfer.setPaused(true);
                    btnTransferRun.setBackground(row.getResources().getDrawable(R.drawable.play));
                }

                transferControl.pauseTransfer(transfer.getTransferID());
            }
        });

        btnTransferExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transferControl.exitTransfer(transfer.getTransferID());
            }
        });

        return  row;

        //return super.getView(position, convertView, parent);
    }
}
