package mcs60.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.List;

import ar.com.daidalos.afiledialog.FileChooserActivity;
import mcs60.common.utility.Util;


public class CreateDistActivity extends Activity {


    private static final String TAG = "CreateDist";

    private final int RESPONSE_FOLDER = 89;

    private Button btnChoose;
    private Button btnConfirm;
    private TextView txtSize;
    private TextView txtNumFiles;
    private TextView txtPath;

    private File folderPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_dist);


        txtSize = (TextView) findViewById(R.id.CreateTxtSize);
        txtNumFiles = (TextView) findViewById(R.id.CreateTxtNumFiles);
        txtPath = (TextView) findViewById(R.id.CreateTxtPath);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_dist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onChoose(View view) {
        Intent i = new Intent(this, FileChooserActivity.class);
        i.putExtra(FileChooserActivity.INPUT_FOLDER_MODE, true);
        startActivityForResult(i, RESPONSE_FOLDER);
    }

    public void onConfirm(View view) {
        Intent i = new Intent();
        i.putExtra("FOLDER_PATH", folderPath);
        setResult(Activity.RESULT_OK, i);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent indent) {

        if (requestCode == RESPONSE_FOLDER) {
            if (resultCode != Activity.RESULT_OK) {
                Toast.makeText(this, "Not Chosen", Toast.LENGTH_SHORT).show();
            }

            Bundle data = indent.getExtras();
            folderPath = (File) data.get(FileChooserActivity.OUTPUT_FILE_OBJECT);

            // compute file details in the folder
            try {
                List<File> files = Util.getFilesInPath(folderPath.toString());
                long totalSize = 0;
                int fileNum = 0;

                for (File fl: files) {
                    totalSize += fl.length();
                    fileNum += 1;
                }


                txtPath.setText(folderPath.toString());
                txtNumFiles.setText(String.valueOf(fileNum));
                txtSize.setText(String.valueOf(totalSize));

            } catch (IOException e) {

                txtPath.setText(folderPath.toString());
                txtNumFiles.setText("Error");
                txtSize.setText("Error");
            }
        }

        super.onActivityResult(requestCode, resultCode, indent);
    }
}
