package mcs60.client;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by dare on 05/07/15.
 */
public class TransferDetails {
    public Thread transferThread;
    AtomicBoolean paused;
    AtomicBoolean exitFlag;

    public TransferDetails(Thread transfer, AtomicBoolean paused,
                           AtomicBoolean exitFlag) {
        this.transferThread = transfer;
        this.paused = paused;
        this.exitFlag = exitFlag;
    }

    public Thread getThread() {
        return transferThread;
    }

    public void pause() {
        paused.set(true);
    }

    public void resume() {
        paused.set(false);
    }

    public boolean isPaused() { return paused.get(); }

    public void exit() { exitFlag.set(true); }
}
