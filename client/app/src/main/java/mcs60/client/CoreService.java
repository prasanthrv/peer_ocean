package mcs60.client;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import mcs60.common.utility.Util;

public class CoreService extends Service {

    private static final String TAG = "CoreService";

    public static final int MSG_CREATE = 45;
    public static final int MSG_ERROR = 50;
    public static final int MSG_FINISH_TRANSFER = 30;
    public static final int MSG_FINISH_PIECE = 80;
    public static final int MSG_START = 100;

    public static final String msgTransfer = "mcs60.client.CoreService.Transfer";

    private String srvAddr;
    private String srvPort;
    private UUID clientID;

    /* paths for storage */
    private final String clientDir = Util.combinePath(Environment.getExternalStorageDirectory().toString(), "MCS60_Client");
    private final String pieceDir  = Util.combinePath(clientDir,"Pieces");
    private final String destDir   = Util.combinePath(clientDir,"Completed");
    private final String distDir   = Util.combinePath(clientDir,"Distribution Files");
    private final String tmpDir    = Util.combinePath(clientDir,"Temp");

    private SharedPreferences settings;

    // Transactions
    Map<UUID, TransferDetails> transfers = new HashMap<UUID, TransferDetails> ();

    /* Create an Intent for communicating to MainActivity */
    private Intent createMessageIntent(String id, int type) {
        Intent i = new Intent();
        i.setAction(msgTransfer);
        i.putExtra("id", id);
        i.putExtra("type", type);
        return i;
    }

    /* Create an intent with extra content field */
    private Intent createMessageIntent(String id, int type, String content) {
        Intent i = createMessageIntent(id, type);
        i.putExtra("content", content);
        return i;
    }

    // Message Handler for worker threads
    private class Handle extends Handler {
        @Override
        public void handleMessage(Message msg) {


            String transferID = (String) msg.getData().get("id");
            String content = (String) msg.getData().get("content");
            switch (msg.what) {

                // the thread has started piece transfer
                case MSG_START: {
                    Intent i = createMessageIntent(transferID, MSG_START, content);
                    i.putExtra("transferType", msg.getData().getString("transferType"));
                    sendBroadcast(i);
                    return;
                }

                case MSG_CREATE: {
                    Log.i(TAG, "Received message from thread: " + transferID);
                    return;
                }

                // the thread has finished transfer
                case MSG_FINISH_TRANSFER: {
                    Log.i(TAG, "Received message from thread: " + transferID);

                    clearTransfer(UUID.fromString(transferID));
                    sendBroadcast(createMessageIntent(transferID, MSG_FINISH_TRANSFER));
                    return;
                }

                // the thread has finished transferring a piece
                case MSG_FINISH_PIECE: {

                    sendBroadcast(createMessageIntent(transferID, MSG_FINISH_PIECE, content));
                    return;
                }
            }
            super.handleMessage(msg);
        }
    }
    private Handler handler = new Handle();



    CoreBinder binder = new CoreBinder();

    private void clearTransfer(UUID id) {
        if (transfers == null) return;

        transfers.remove(id);
        System.gc();
    }

    private ServerConnection getNewConnection () {
        return new ServerConnection(srvAddr,Integer.valueOf(srvPort),TAG,clientID);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        settings = PreferenceManager.getDefaultSharedPreferences(this);

        validateClientID();
        validateFolderPaths();
        clearDirectoryThreaded(tmpDir);

    }

    public void setServerInfo(String address, String port) {
        this.srvAddr = address;
        this.srvPort = port;
    }

    // authorize the username and password with the server
    public boolean authorize(String userName, String pwd) {

        boolean authorized = false;
        // need network access to be synchronous
        StrictMode.ThreadPolicy oldPolicy = StrictMode.getThreadPolicy();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Log.i(TAG,"Authorizing");

        ServerConnection conn = new ServerConnection(srvAddr, Integer.valueOf(srvPort),
                TAG, clientID);

        if(conn.connect()) {
            UUID authID = conn.sendAuthMessage(userName, pwd);
             if (conn.receivedACK(authID)) {
                 String info = conn.receiveInfo();
                 if (info.equals("Valid"))
                     authorized = true;
             }
        }

        StrictMode.setThreadPolicy(oldPolicy);
        return authorized;
    }

    // Create upload transaction
    public boolean createUploadTransfer(String folderPath) {

        AtomicBoolean paused = new AtomicBoolean(false);
        AtomicBoolean exitFlag = new AtomicBoolean(false);
        UUID transferID = UUID.randomUUID();
        TransferDetails upload = new TransferDetails(new Thread(new UploadTransfer(folderPath,tmpDir,
                distDir,srvAddr, srvPort, clientID, handler, transferID, paused, exitFlag)), paused, exitFlag);
        transfers.put(transferID, upload);
        transfers.get(transferID).getThread().start();



        return true;

    }

    // Create download transfer
    public boolean createDownloadTransfer(String distfile) {

        AtomicBoolean paused = new AtomicBoolean(false);
        AtomicBoolean exitFlag = new AtomicBoolean(false);
        UUID transferID = UUID.randomUUID();
        TransferDetails download = new TransferDetails(new Thread(new DownloadTransfer(pieceDir, destDir, distfile,
                transferID, srvAddr, srvPort, clientID, handler, paused, exitFlag)), paused, exitFlag);
        transfers.put(transferID, download);
        transfers.get(transferID).getThread().start();

        return true;
    }

    public CoreService() {
    }

    /* retrieve client ID or create one */
    public void validateClientID() {



        String clientID;
        clientID =  settings.getString("clientID", "");
        if(clientID.equals("")) {
            clientID = UUID.randomUUID().toString();
            SharedPreferences.Editor settingsEditor = settings.edit();
            settingsEditor.putString("clientID", clientID);
            settingsEditor.commit();
        }
        this.clientID = UUID.fromString(clientID);

    }

    /* check for needed directories and if not present, create them */
    public void validateFolderPaths() {


        File flClientDir = new File(clientDir);
        File flPieceDir  = new File(pieceDir);
        File flDestDir   = new File(destDir);
        File flDistDir   = new File(distDir);
        File flTmpDir    = new File(tmpDir);

        try {
            if (!flClientDir.exists()) { flClientDir.mkdir(); }
            if (!flPieceDir.exists())  { flPieceDir.mkdir();  }
            if (!flDestDir.exists())   { flDestDir.mkdir();   }
            if (!flDistDir.exists())   { flDistDir.mkdir();   }
            if (!flTmpDir.exists())    { flTmpDir.mkdir();   }
        } catch (Exception e) {

            Log.e(TAG, e.getStackTrace().toString());
            throw new RuntimeException(e.getMessage());
        }
    }


    /* Recursively delete contents of a directory in a new thread */
    private void clearDirectoryThreaded(final String dir) {
        new Thread(new Runnable() {

            void deleteDir(String path) {
                File flTmpDir = new File(path);

                if(flTmpDir.isDirectory()) {
                    for (File fl : flTmpDir.listFiles()) {
                        if (fl.isDirectory()) { deleteDir(fl.getPath()); }
                        fl.delete();
                    }
                }
            }
            @Override
            public void run() {
                deleteDir(dir);

            }
        }).run();

    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "Binded");
        return binder;
    }

    /*
    Binder to return to calling activities
     */
    public class CoreBinder extends Binder {
        CoreService getService() {
            return CoreService.this;
        }
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Destroyed");
        super.onDestroy();
    }

    /* pause a transfer */
    public void pauseTransfer(UUID id) {
        for(Map.Entry<UUID, TransferDetails> item : transfers.entrySet()) {
            if (item.getKey().equals(id)){
                if (item.getValue().isPaused())
                    item.getValue().resume();
                else
                    item.getValue().pause();
            }
        }
    }

    /* exit a transfer */
    public void exitTransfer(UUID id) {
        for (Map.Entry<UUID, TransferDetails> item : transfers.entrySet()) {
            if (item.getKey().equals(id)) {
                item.getValue().exit();
            }
        }
    }

    public void pauseTest() {

        for (TransferDetails detail : transfers.values()) {
            if (detail.isPaused())
                detail.resume();
            else
                detail.pause();
        }

    }

    public void exitTest() {
        for (TransferDetails detail : transfers.values())
            detail.exit();
    }
}
