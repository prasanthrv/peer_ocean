package mcs60.client;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Stack;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import mcs60.common.bundle.DistJson;
import mcs60.common.bundle.DistPackage;
import mcs60.common.bundle.DistPackageGenerator;
import mcs60.common.bundle.DistPackagePieceFileGenerator;
import mcs60.common.bundle.Piece;
import mcs60.common.utility.Util;

/**
 * Created by dare on 04/07/15.
 */
public class UploadTransfer extends Transfer implements Runnable {

    private String srcPath;
    private String tmpPath;
    private String distDirPath;
    protected Stack<File> remainingPieceFiles ;


    public UploadTransfer(String srcPath, String tmpPath, String distPath, String srvAddr, String srvPort, UUID clientID, Handler handle,
                          UUID transferID, AtomicBoolean refPaused, AtomicBoolean exitFlag) {
        super(clientID, srvPort, srvAddr, transferID, handle, refPaused, exitFlag);
        this.srcPath = srcPath;
        this.tmpPath = tmpPath;
        this.distDirPath = distPath;

        this.isPaused = refPaused;

        TAG = "UploadTransfer:" + transferID.toString();
    }


    /* get the pieces in the current directory and push onto a stack */
    private boolean populateFiles(String srcDir) {

        remainingPieceFiles = new Stack<File>();

        try {
            for (File fl : Util.getFilesInPath(srcDir)) {
                remainingPieceFiles.push(fl);
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            Log.e(TAG, e.getStackTrace().toString());
            return false;

        }
        return true;

    }


    /* transfer the pieces */
    private boolean transferPieces() {

        conn = getNewConnection();

        try {
            boolean allPiecesSent = true;
            byte[] buf;
            Piece pieceInfo;
            int piecesDone = 0;
            while (!remainingPieceFiles.isEmpty()) {

                // exit if flag is set
                if (exitFlag.get()) {
                    sendMsg("Exit flag set");
                    return false;
                }

                // if paused, then sleep
                sleepIfPaused();

                // if the connection has become invalid, create a new one
                if (!conn.isValid()) {
                    conn = getNewConnection();
                    if (conn == null)
                        return false;
                }


                // transfer the next piece
                File fl = remainingPieceFiles.pop();

                buf = FileUtils.readFileToByteArray(fl);
                pieceInfo = new Piece(1, fl.getName(), "");
                int retry = 3;
                boolean transferSuccess = false;
                while (retry > 0) {
                    try {
                        UUID msgID = conn.sendPieceContent(pieceInfo, buf);
                        if (conn.receivedACK(msgID)) {
                            transferSuccess = true;
                            break;
                        }
                    } catch (Exception e) {
                        retry--;
                    }
                }

                if (!transferSuccess) {
                    allPiecesSent = false;
                    break;
                }
                sendMsgPiece(++piecesDone);
                buf = null;
                pieceInfo = null;
            }

            if(allPiecesSent)
                return true;
        } catch (IOException e) {
           sendMsg("Could not read a file");
        }

        return false;

    }

    @Override
    public void run() {

        try {


            dist = DistPackageGenerator.generate(srcPath);
            sendMsg("Created dist file");


            // create a new directory in tmp folder with transfer ID as name
            String pieceTmpPath = Util.combinePath(tmpPath, transferID.toString());
            if (! new File(pieceTmpPath).mkdir()) {
                sendMsg("Cannot create temp directory");
                return;
            }

            DistPackagePieceFileGenerator.generate(pieceTmpPath, dist);
            sendMsg("Created file pieces");



            // begin piece transfer
            populateFiles(pieceTmpPath);
            sendMsgStart("Upload", remainingPieceFiles.size());
            if (transferPieces()) {
                String jsonData = DistJson.convertToJSON(dist);
                File distJsonFile = new File (Util.combinePath(distDirPath, transferID.toString() + ".dist"));
                FileUtils.write(distJsonFile, jsonData);
                Util.deleteDir(pieceTmpPath);
                sendMsgFinish("Finished upload");
            }
            else
                sendMsgFinish("Could not transfer all pieces");

        } catch (IOException e) {
            sendMsgFinish("Could not upload files");
        }
    }
}
