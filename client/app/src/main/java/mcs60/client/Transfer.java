package mcs60.client;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.File;
import java.util.Stack;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import mcs60.common.bundle.DistPackage;

/**
 * Created by dare on 07/07/15.
 */
public class Transfer {
    protected ServerConnection conn;
    protected Handler handle;
    protected UUID transferID;
    protected String srvAddr;
    protected int srvPort;
    protected UUID clientID;
    protected String TAG;
    DistPackage dist;

    protected AtomicBoolean isPaused;
    protected AtomicBoolean exitFlag;

    public Transfer(UUID clientID, String srvPort, String srvAddr, UUID transferID,
                    Handler handle, AtomicBoolean isPaused, AtomicBoolean exitFlag) {
        this.clientID = clientID;
        this.srvPort = Integer.valueOf(srvPort);
        this.srvAddr = srvAddr;
        this.transferID = transferID;
        this.handle = handle;
        this.isPaused  = isPaused;
        this.exitFlag = exitFlag;
    }

    /* create normal bundle for message */
    private Bundle getBundleMessage( int what, String strMsg) {

        Bundle data = new Bundle();
        data.putString("id", transferID.toString());
        data.putString("content", strMsg);
        return data;
    }

    /* send message on transfer error */
    protected void sendMsgError(String errMsg) {
        sendMsgWhat(CoreService.MSG_ERROR, errMsg);
    }

    /* send message on transfer start */
    protected void sendMsgStart(String type, int piecesTotal) {
        Bundle data = getBundleMessage(CoreService.MSG_START, String.valueOf(piecesTotal));
        data.putString("transferType", type);
        sendMessageBundle(data, CoreService.MSG_START);
    }

    /* send normal message */
    protected void sendMsg(String strMsg) {
        sendMsgWhat(CoreService.MSG_CREATE, strMsg);
    }

    /* send message on transfer complete */
    protected void sendMsgFinish(String strMsg) {
        sendMsgWhat(CoreService.MSG_FINISH_TRANSFER, strMsg);
    }

    /* send message on piece completion */
    protected void sendMsgPiece(int piecesFinished) {
        sendMsgWhat(CoreService.MSG_FINISH_PIECE, String.valueOf(piecesFinished));
    }

    /* given a message type and content, send message */
    private void sendMsgWhat(int what, String strMsg) {

        Bundle data = getBundleMessage(what, strMsg);
        sendMessageBundle(data, what);

    }

    /* send bundle as message to handler */
    private void sendMessageBundle(Bundle data, int what) {
        Message msg = Message.obtain(handle,what,0,0, data);
        msg.setData(data);
        msg.sendToTarget();

    }

    /* Get a new ServerConnection */
    public ServerConnection getNewConnection() {
        conn = new ServerConnection(srvAddr, srvPort,TAG,clientID);
        if (!conn.connect()) {
            Log.e(TAG, "Cannot connect to server");
            sendMsg("Could not connect to server");
            return null;
        }
        return conn;
    }

    /* make the thread sleep if isPaused true */
    public void sleepIfPaused() {
        if (isPaused.get()) {
            conn.close();
        }
       while (isPaused.get())
           try{
               Thread.sleep(500);
           } catch (Exception e) {
               Log.e(TAG, "Interrupted");
           }
    }

}
