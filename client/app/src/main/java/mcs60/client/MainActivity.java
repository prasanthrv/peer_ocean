package mcs60.client;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import ar.com.daidalos.afiledialog.FileChooserActivity;


public class MainActivity extends Activity  implements  TransferControl{

    @Override
    public void pauseTransfer(UUID id) {
        core.pauseTransfer(id);
    }


    @Override
    public void exitTransfer(UUID id) {
        core.exitTransfer(id);
        for (TransferRowDetail transfer : transfers) {
            if (transfer.getTransferID().equals(id)) {
                transfers.remove(transfer);
            }
        }
        transferAdapter.notifyDataSetChanged();
    }

    private static final String TAG = "MainActivity";

    private boolean loggedIn = true;

    private static final int RESULT_DIST_FOLDER = 588;
    private static final int RESULT_PICK_DIST = 400;

    private MessageUpdate messageReceiver;

    CoreService core;
    private boolean isBound;

    ListView transferList;
    TransferAdapter transferAdapter;

    List<TransferRowDetail> transfers;

    private class MessageUpdate extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            switch (bundle.getInt("type")) {

                // a transfer has completed successfully
                case CoreService.MSG_FINISH_TRANSFER: {
                    Log.i(TAG, "Got finish broadcast");
                    UUID id = UUID.fromString(bundle.getString("id"));
                    for(TransferRowDetail transfer: transfers) {
                        if (transfer.getTransferID().equals(id)){
                            transfer.setFinished(true);
                            Toast.makeText(MainActivity.this,"Finished " + bundle.getString("id"),Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                    transferAdapter.notifyDataSetChanged();
                    // other cleanup
                    break;
                }

                // a piece has been transferred
                case CoreService.MSG_FINISH_PIECE: {
                    UUID id = UUID.fromString(bundle.getString("id"));
                    for(TransferRowDetail transfer: transfers) {
                        if (transfer.getTransferID().equals(id)){
                            transfer.setPiecesDone(Integer.valueOf(bundle.getString("content")));
                            break;
                        }
                    }
                    transferAdapter.notifyDataSetChanged();
                    break;
                }

                // a transfer has started
                case CoreService.MSG_START: {
                    String transferID = bundle.getString("id");
                    int totalPieces = Integer.valueOf(bundle.getString("content"));
                    String type = bundle.getString("transferType");
                    transfers.add(new TransferRowDetail(UUID.fromString(transferID), totalPieces, type));
                    transferAdapter.notifyDataSetChanged();
                }
            }

        }
    }

    @Override
    protected void onPause() {
        if(messageReceiver != null)
            unregisterReceiver(messageReceiver);

        super.onPause();
    }

    @Override
    protected void onResume() {
        if(messageReceiver == null)
            messageReceiver = new MessageUpdate();
        IntentFilter intentFilter = new IntentFilter(CoreService.msgTransfer);
        registerReceiver(messageReceiver, intentFilter);

        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        transfers = new LinkedList<TransferRowDetail>();

        Intent inSrv = new Intent(this, CoreService.class);
        bindService(inSrv, coreConnection, Context.BIND_AUTO_CREATE);
        transferList = (ListView) findViewById(R.id.transferList);

        transferAdapter = new TransferAdapter(this, 0, transfers, this);
        transferList.setAdapter(transferAdapter);

    }

    @Override
    public void onBackPressed() {
        if(loggedIn)
            return;
        else
            super.onBackPressed();
    }

    public void showCreateDist(View view) {

        Intent i = new Intent(this, CreateDistActivity.class);
        startActivityForResult(i, RESULT_DIST_FOLDER);

    }

    public void showPickDist(View view) {

        Intent i = new Intent(this, FileChooserActivity.class);
        startActivityForResult(i, RESULT_PICK_DIST);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            // picked a folder for creating a dist file
            case RESULT_DIST_FOLDER: {

                if (resultCode == Activity.RESULT_OK) {
                    File folder = (File) data.getExtras().get("FOLDER_PATH");
                    if (folder != null) {
                        Log.i(TAG, "Folder path for create dist:  " + folder.toString());
                        core.createUploadTransfer(folder.toString());
                    }
                    else
                        Log.i(TAG, "No folder selected");
                }

            }
            break;

            //picked a dist file to add
            case RESULT_PICK_DIST: {

                if (resultCode == Activity.RESULT_OK) {
                    File distFile = (File) data.getExtras().get(FileChooserActivity.OUTPUT_FILE_OBJECT);

                    if (distFile != null) {
                        Log.i(TAG, "Selected dist file: " + distFile.toString());
                        core.createDownloadTransfer(distFile.getPath());
                    } else {
                        Log.i(TAG, "No file selected");
                    }

                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    //CoreService Connection
    private ServiceConnection coreConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            CoreService.CoreBinder binder = (CoreService.CoreBinder) iBinder;
            core = binder.getService();
            isBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBound = false;
        }
    };

    public void onBtnPause(View view) {
        core.pauseTest();
    }

    public void onBtnExit(View view) { core.exitTest(); }
}
