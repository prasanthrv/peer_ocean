package mcs60.client;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class GatewayActivity extends Activity {


    private final static String TAG = "GatewayActivity";

    private EditText txtAddress;
    private EditText txtPort;

    private String srvAddr;
    private String srvPort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gateway);

        // get edit texts
        txtAddress = (EditText) findViewById(R.id.GatewayEdtIPaddr);
        txtPort = (EditText) findViewById(R.id.GatewayEdtPort);

        // get data from indent
        Intent i = getIntent();
        srvAddr = i.getStringExtra("srvAddr");
        srvPort = i.getStringExtra("srvPort");
        txtAddress.setText(srvAddr);
        txtPort.setText(srvPort);


    }

    public void updateGatewayInfo(View v) {
        String addr = txtAddress.getText().toString();
        String port = txtPort.getText().toString();

        // return updated values
        if (!addr.equals("") && !port.equals(""))  {
            Intent i = new Intent();
            i.putExtra("resultAddr", addr);
            i.putExtra("resultPort", port);
            setResult(Activity.RESULT_OK,i);
            finish();
       }
    }





}
