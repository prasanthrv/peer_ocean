package mcs60.client;

import android.os.Handler;

import org.apache.commons.io.FileUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import mcs60.common.bundle.DistJson;
import mcs60.common.bundle.DistPackage;
import mcs60.common.bundle.DistPackageAssembler;
import mcs60.common.bundle.FileAttribute;
import mcs60.common.bundle.Piece;
import mcs60.common.utility.Util;

/**
 * Created by dare on 07/07/15.
 */
public class DownloadTransfer extends Transfer implements Runnable{
    private String piecePath;
    private String destPath;
    private String distPath;

    private DistPackage dist;
    private LinkedList<Piece> remainingPieces;



    public DownloadTransfer(String piecePath, String destPath, String distPath, UUID transferID,
                            String srvAddr, String srvPort, UUID clientID,
                            Handler handle, AtomicBoolean isPaused, AtomicBoolean exitFlag) {
        super(clientID, srvPort, srvAddr, transferID, handle, isPaused, exitFlag);
        this.piecePath = piecePath;
        this.destPath = destPath;
        this.distPath = distPath;

        this.TAG = "DownloadTransfer:" + transferID.toString();
    }

    /* populate all pieces needed to download */
    private void populatePieces() {

        remainingPieces = new LinkedList<Piece>();

        for(FileAttribute fileAttr : dist.getFileInfo()) {
            for(Piece piece : fileAttr.getPieceInfo()) {
                remainingPieces.push(piece);
            }
        }

    }

    private boolean downloadStart() {


        conn = getNewConnection();

        try {

            boolean pieceReceived = true;
            int piecesDone = 0;

            // get pieces from server
            while (!remainingPieces.isEmpty()) {


                // exit if flag is set
                if (exitFlag.get()) {
                    sendMsg("Exit flag set");
                    return false;
                }

                sleepIfPaused();

                if (!conn.isValid()) {
                    conn = getNewConnection();
                    if (conn == null)
                        return false;
                }


                boolean transferSuccess = false;

                    Piece currentPiece = remainingPieces.getFirst();
                    String filePath = Util.combinePath(piecePath, currentPiece.getHashOne());
                    if(Util.validateFile(filePath) && filePath.length() >=0 ) {
                        remainingPieces.pop();
                        continue;
                    }

                    UUID msgID = conn.sendPieceRequest(currentPiece);
                    if (msgID == null)
                        continue;

                    if (conn.receivedACK(msgID)) {

                        byte[] buf = conn.receivePieceData();

                        if (buf == null) {
                            try {
                                Thread.sleep(1000);
                                remainingPieces.add(remainingPieces.pop());
                            } catch (InterruptedException e) {
                                ;
                            }
                            continue;
                        }

                        /*
                        int pieceLength = conn.receivePieceLength();
                        if (pieceLength == 0) {
                            try {
                                Thread.sleep(1000);
                                remainingPieces.add(remainingPieces.pop());
                            } catch (InterruptedException e) {
                                ;
                            }
                            continue;
                        }

                        conn.receivePiece(pieceLength, filePath);
                        */
                         FileUtils.write(new File(filePath), new String(buf));

                        sendMsgPiece(++piecesDone);
                        remainingPieces.pop();
                    }
            }


        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public void run() {

        try {

            File distFile = new File(distPath);
            String distHash = Util.hashMD5(FileUtils.readFileToByteArray(distFile));
            dist = DistJson.convertFromJSON(FileUtils.readFileToString(distFile));

            // convert dist file to object
            if (dist != null)
                sendMsg("Converted to dist object");
            else {
                sendMsg("Could not create dist object");
                return;
            }

            //create temporary and destination directories
            piecePath = Util.combinePath(piecePath, distHash);
            destPath = Util.combinePath(destPath, String.valueOf(distFile.getName()));

            if (!createDirectory(piecePath)) return;
            if (!createDirectory(destPath)) return;

            populatePieces();
            sendMsgStart("Download", remainingPieces.size());

            if (downloadStart())
                sendMsg("Downloaded pieces");
            else {
                sendMsgFinish("Could not download pieces");
                return;
            }

            //assemble the pieces
            if (DistPackageAssembler.assemble(dist, destPath, piecePath)) {
                sendMsg("Assembled original files");
                Util.deleteDir(piecePath);
                sendMsgFinish("Finished download, " + transferID);
            }
            else {
                sendMsg("Could not assemble files");
                sendMsgFinish("Could not complete download transfer");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /* Validate and if not present, create directory */
    private boolean createDirectory(String dirPath) {

        if (Util.validateDirectory(dirPath))
            sendMsg("Directory already created, " + dirPath);
        else
            if (new File(dirPath).mkdir())
                sendMsg("Directory created, " + dirPath);
            else {
                sendMsg("Could not create directory, " + dirPath);
                return false;
        }

        return true;
    }


}
