package mcs60.client;

import java.util.UUID;

/**
 * Created by dare on 7/26/2015.
 */
public interface TransferControl {
    public void pauseTransfer(UUID id);
    public void exitTransfer(UUID id);
}
