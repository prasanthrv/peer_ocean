package mcs60.serverTest.tests;

import mcs60.common.bundle.Piece;
import mcs60.common.protocol.messages.*;
import mcs60.common.utility.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.UUID;

/**
 * Created by dare on 08/05/15.
 */
public class SampleClient {

    /** Logger */
    private static final Logger log = LogManager.getRootLogger();

    private Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private MessageFrame msg;
    private UUID curMsgID;


    private UUID clientID;
    private int timeout;

    public UUID getClientID() {
        return clientID;
    }

    public void init(int port) throws IOException {

        timeout = 4 * 1000; // set timeout
        log.trace("intializing to port " + port);
        clientID = UUID.randomUUID();
        socket = new Socket("0.0.0.0",port);

        /*
        try {
            socket.setSoTimeout(timeout);
        } catch (IOException e) {
            log.error("Cannot set timeout");
        }
        */

        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());

    }

    public void sendEcho() throws Exception {


            try {
                log.trace("Sending echo..");
                msg = new EchoMessage(clientID);
                curMsgID = msg.getMsgID();
                out.writeObject(msg);
                out.flush();
            } catch (SocketException e) {
                log.error ("Cannot send echo.. socket closed");
            } catch (IOException e) {
                log.error("Cannot send echo.. IOException");
            }
    }

    public void sendExit() throws Exception {


            try {
                log.trace("Sending exit..");
                msg = new ExitMessage(clientID);
                curMsgID = msg.getMsgID();
                out.writeObject(msg);
                out.flush();
            } catch (SocketException e) {
                log.error ("Cannot send exit message.. socket closed");
            } catch (IOException e) {
                log.error("Cannot send exit message.. IOException");
            }
    }

    public void sendInfo(String info) throws Exception {


            try {
                log.trace("Sending info..");
                msg = new InfoMessage(info,clientID);
                curMsgID = msg.getMsgID();
                out.writeObject(msg);
                out.flush();
            } catch (SocketException e) {
                log.error ("Cannot send info message.. socket closed");
            } catch (IOException e) {
                log.error("Cannot send info message.. IOException");
            }
    }

    public void sendPieceRequest(Piece piece) throws Exception {


        try {
            log.trace("Sending piece request..");
            msg = new PieceRequestMessage(piece, clientID);
            curMsgID = msg.getMsgID();
            out.writeObject(msg);
            out.flush();
        } catch (SocketException e) {
            log.error ("Cannot send piece request message.. socket closed");
        } catch (IOException e) {
            log.error("Cannot send piece request message.. IOException");
        }
    }

    public void sendPieceContent(Piece piece, byte[] data) throws Exception {


        try {
            log.trace("Sending piece request..");
            msg = new PieceContentMessage(piece, data, clientID);
            curMsgID = msg.getMsgID();
            out.writeObject(msg);
            out.flush();
        } catch (SocketException e) {
            log.error ("Cannot send piece request message.. socket closed");
        } catch (IOException e) {
            log.error("Cannot send piece request message.. IOException");
        }
    }


    public void sendAuthMessage(String username, String pwd) throws Exception{
        try {
            log.trace("Sending authentication message...");
            msg = new AuthMessage(Util.hashSHA256(username.getBytes()),
                    Util.hashSHA256(pwd.getBytes()), clientID);
            curMsgID = msg.getMsgID();
            out.writeObject(msg);
            out.flush();
        } catch (SocketException e) {
            log.error ("Cannot send piece request message.. socket closed");
            throw e;
        } catch (IOException e) {
            log.error("Cannot send piece request message.. IOException");
            throw e;
        }
    }

    /* the client received ACK to the previous message */
    public boolean receivedACK() throws Exception{

        while(socket.isConnected()) {
            try {
                msg = (MessageFrame) in.readObject();
                if (msg.getRequestType() == MessageFrame.MESSAGE_TYPE.ACK &&
                        curMsgID.equals(msg.getMsgID())) {
                    return true;
                }
            } catch (SocketTimeoutException sc) {
                log.error("Socket timed out");
                break;
            } catch (IOException e) {
                log.error("IO error in acknowlegment confirmation");
                break;
            } catch (NullPointerException e) {
                log.error("Current Message ID is null");
                throw e;
            }

        }

        return false;
    }

    /* get piece data transferred by the server */
    public byte[] receivePieceData() throws Exception{

        PieceContentMessage msg;
        while(socket.isConnected()) {
            try {
                msg = (PieceContentMessage) in.readObject();
                return msg.getData();
            } catch (SocketTimeoutException sc) {
                log.error("Socket timed out");
            } catch (IOException e) {
                log.error("IO error in acknowlegment confirmation");
            } catch (ClassCastException e) {
                log.error("Cannot cast message to PieceContentMessage");
            }

        }
        return null;

    }

    public String receiveInfo() throws Exception {
        InfoMessage msg;
        while(socket.isConnected()) {
            try {
                msg = (InfoMessage) in.readObject();
                return msg.getInfo();
            } catch (SocketTimeoutException sc) {
                log.error("Socket timed out");
            } catch (IOException e) {
                log.error("IO error in acknowlegment confirmation");
            } catch (ClassCastException e) {
                log.error("Cannot cast message to PieceContentMessage");
            }

        }
        return null;
    }


    public UUID getCurMsgID() {
        return curMsgID;
    }

    public void close() throws Exception {
        socket.shutdownInput();
        socket.shutdownOutput();
        socket.close();
    }
}
