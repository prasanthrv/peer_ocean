package mcs60.serverTest.tests;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import mcs60.common.bundle.Piece;
import mcs60.common.utility.Util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by dare on 11/05/15.
 */
public class SampleClientTest {

    private static String rootPath = System.getProperty("user.dir");
    private static Path logPath =  Paths.get(rootPath, "log", "log.txt");
    Path serverLogPath = Paths.get("/Users/dare/projs/mcs60/server/log/log.txt");
    Path clientPiecePath = Paths.get("/Users/dare/projs/mcs60/serverTest/pieces");
    Path serverPiecePath = Paths.get("/Users/dare/projs/mcs60/server/pieces");

    class PieceDebug {
        public String hash;
        public long size;

        public PieceDebug(String hash, long size) {
            this.hash = hash;
            this.size = size;
        }

        public PieceDebug(String hash) {
            this.hash = hash;
        }
    }

    private static List<PieceDebug> pieceSent;
    private static String curMsgID;


    private SampleClient client;

    /* test if server logs a message with client id */
    private void assertTextInServerLog(String text, UUID id) throws Throwable {
        Thread.sleep(100);
        String logText = new String(Files.readAllBytes(serverLogPath),
                StandardCharsets.UTF_8);
        assert (logText.contains(text));
        assert (logText.contains(id.toString()));
    }

    /* test if server logs a message */
    private void assertTextInServerLog(String text) throws Throwable {
        Thread.sleep(100);
        String logText = new String(Files.readAllBytes(serverLogPath),
                StandardCharsets.UTF_8);
        assert (logText.contains(text));
    }

    @Given("^The Server running$")
    public void the_Server_running() throws Throwable {

        client = new SampleClient();

        try {
            client.init(23450);
            //client.close();
        } catch (IOException e) {
            fail();
        }
    }

    @When("^Client sends an ECHO message$")
    public void client_sends_an_ECHO_message() throws Throwable {
        //client.init(23450);
        client.sendEcho();

    }

    @Then("^Server receives the ECHO message$")
    public void server_receives_the_ECHO_message() throws Throwable {

        assertTextInServerLog("Got an ECHO message", client.getClientID());
    }

    @Then("^Client receives ACK message$")
    public void client_receives_ACK_message() throws Throwable {
        //System.out.println(client.receivedACK());
        assert(client.receivedACK());
    }

    @Then("^Client closes connection$")
    public void client_closes_connection() throws Throwable {
        client.close();
    }

    @Then("^Server closes connection with timeout$")
    public void server_closes_connection_with_timeout() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Thread.sleep(3500);
        assertTextInServerLog("Thread ended");
    }

    @Then("^Client sends an EXIT message$")
    public void client_sends_an_EXIT_message() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        client.sendExit();
    }

    @Then("^Server closes connection$")
    public void server_closes_connection() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        assertTextInServerLog("Thread ended");
    }

    @Then("^Server receives the EXIT message$")
    public void server_receives_the_EXIT_message() throws Throwable {
        assertTextInServerLog("Got an EXIT message", client.getClientID());
    }

    @When("^Client sends INFO message$")
    public void client_sends_INFO_message() throws Throwable {
        String message = "This is a test info message";
        client.sendInfo(message);
    }

    @Then("^Server receives INFO message$")
    public void server_receives_INFO_message() throws Throwable {
        assertTextInServerLog("Got an INFO message", client.getClientID());
    }

    @Then("^Server is able to log INFO$")
    public void server_is_able_to_log_INFO() throws Throwable {
        String message = "This is a test info message";
        assertTextInServerLog(message);
    }

    @Then("^No error is raised$")
    public void no_error_is_raised() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        ;
    }

    @When("^Client sends a PieceRequest message$")
    public void client_sends_a_PieceRequest_message() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        List<File> files = Util.getFilesInPath(clientPiecePath.toString());
        File pieceFile = files.get(0);
        Piece testPiece = new Piece(456,pieceFile.getName(),"");

        client.sendPieceRequest(testPiece);
    }

    @Then("^Server receives the PieceRequest message$")
    public void server_receives_the_PieceRequest_message() throws Throwable {
        assertTextInServerLog("Got a PIECE REQUEST message");
    }

    @Then("^Server is able to log details of the PieceRequest$")
    public void server_is_able_to_log_details_of_the_PieceRequest() throws Throwable {
        List<File> files = Util.getFilesInPath(clientPiecePath.toString());
        File pieceFile = files.get(0);
        assertTextInServerLog("Piece Hash: " + pieceFile.getName());
    }

    @When("^Client transfers a piece$")
    public void client_transfers_a_piece() throws Throwable {
        List<File> files = Util.getFilesInPath(clientPiecePath.toString());
        File pieceFile = files.get(0);

        pieceSent = new LinkedList<PieceDebug>();
        pieceSent.add(new PieceDebug(pieceFile.getName(),
                pieceFile.length()));

        Piece pieceInfo = new Piece(456,pieceFile.getName(),"");

        byte[] data = Files.readAllBytes(pieceFile.toPath());
        client.sendPieceContent(pieceInfo, data);
    }

    @Then("^Server receives the PieceContent message$")
    public void server_receives_the_PieceContent_message() throws Throwable {
        assertTextInServerLog("Wrote piece to file:");
    }

    @Then("^The piece is written as a file$")
    public void the_piece_is_written_as_a_file() throws Throwable {
        Path filePath = Paths.get(serverPiecePath.toString(),
                pieceSent.get(0).hash);
        assert(Files.exists(filePath));
        assertEquals(new File(filePath.toString()).length(),
                pieceSent.get(0).size);

    }

    @When("^Client transfers multiple pieces$")
    public void client_transfers_multiple_pieces() throws Throwable {

        List<File> files = Util.getFilesInPath(clientPiecePath.toString());
        pieceSent = null;
        pieceSent = new LinkedList<PieceDebug>();

        for(File file: files) {

            Piece pieceInfo = new Piece(456,file.getName(),"");
            byte[] data = Files.readAllBytes(file.toPath());
            client.sendPieceContent(pieceInfo, data);
            pieceSent.add(new PieceDebug(file.getName(), file.length()));
        }
    }

    @Then("^All the pieces are written to files$")
    public void all_the_pieces_are_written_to_files() throws Throwable {
        for (PieceDebug piece: pieceSent) {

            Path filePath = Paths.get(serverPiecePath.toString(), piece.hash);
            assert(Files.exists(filePath));
            assertEquals(Files.size(filePath), piece.size);
            //System.out.println(Files.size(filePath)+ ", " + piece.size);
        }
    }

    @Then("^Client receives PieceContentMessage with Piece$")
    public void client_receives_PieceContentMessage_with_Piece() throws Throwable {
        List<File> files = Util.getFilesInPath(clientPiecePath.toString());
        File pieceFile = files.get(0);
        byte[] data = Files.readAllBytes(pieceFile.toPath());

        assert(Arrays.equals(data, client.receivePieceData()));
    }

    @When("^Client sends a Authentication Message with invalid data$")
    public void client_sends_a_Authentication_Message_with_invalid_data() throws Throwable {
        client.sendAuthMessage("xxx", "yyy");
    }

    @Then("^Server receives Auth Message$")
    public void server_receives_Auth_Message() throws Throwable {
        assertTextInServerLog("Received an AuthMessage");
    }

    @Then("^Client receives Invalid InfoMessage$")
    public void client_receives_Invalid_InfoMessage() throws Throwable {
        String info = client.receiveInfo();
        assert(info.equals("Invalid"));
    }

    @Then("^Client receives Valid InfoMessage$")
    public void client_receives_Valid_InfoMessage() throws Throwable {
        String info = client.receiveInfo();
        assert(info.equals("Valid"));
    }

    @When("^Client sends a Authentication Message with valid data$")
    public void client_sends_a_Authentication_Message_with_valid_data() throws Throwable {
        client.sendAuthMessage("raghav", "mbti");
    }
}
