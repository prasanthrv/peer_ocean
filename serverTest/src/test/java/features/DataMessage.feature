Feature: Test the various message handling capabilities of server

  Scenario: Test if server can extract info from a INFO Message
    Given The Server running
    When Client sends INFO message
    Then Server receives INFO message
    And Server is able to log INFO
    And Client receives ACK message
    And Client closes connection




