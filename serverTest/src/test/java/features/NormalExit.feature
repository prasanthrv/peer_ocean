Feature: Test Basic Connection

  Scenario: Test an ECHO message followed by EXIT message
    Given The Server running
    When Client sends an ECHO message
    Then Server receives the ECHO message
    And Client receives ACK message
    And Client sends an EXIT message
    Then Server receives the EXIT message
    And Client receives ACK message
    And Server closes connection






