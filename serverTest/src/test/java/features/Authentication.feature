Feature: Test the authentication features of the server

  Scenario: Test invalid authentication details
    Given The Server running
    When Client sends a Authentication Message with invalid data
    Then Server receives Auth Message
    And Client receives ACK message
    And Client receives Invalid InfoMessage
    And Client closes connection

  Scenario: Test valid authentication details
    Given The Server running
    When Client sends a Authentication Message with valid data
    Then Server receives Auth Message
    And Client receives ACK message
    And Client receives Valid InfoMessage
    And Client closes connection

