Feature: Test Timeout of connection to server

  Scenario: Test an echo message from client to server
    Given The Server running
    When Client sends an ECHO message
    Then Server receives the ECHO message
    And Client receives ACK message
    And Client closes connection
    And Server closes connection with timeout


  Scenario: Test an echo message with remote client close
    Given The Server running
    When Client sends an ECHO message
    And Client closes connection
    Then Server receives the ECHO message
    And Server closes connection with timeout



