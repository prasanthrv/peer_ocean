Feature: To test the piece data transfer feature of the server

  Scenario: Test if server can extract piece request from message
    Given The Server running
    When Client sends a PieceRequest message
    Then Server receives the PieceRequest message
    And Server is able to log details of the PieceRequest
    And Client receives ACK message
    And Client closes connection

  Scenario: Transfer a piece
    Given The Server running
    When Client transfers a piece
    Then Server receives the PieceContent message
    Then The piece is written as a file
    And Client receives ACK message


  Scenario: Transfer multiple pieces
    Given The Server running
    When Client transfers multiple pieces
    Then All the pieces are written to files


  Scenario: Test request and piece transfer
    Given The Server running
    When Client sends a PieceRequest message
    Then Server is able to log details of the PieceRequest
    And Client receives ACK message
    And Client receives PieceContentMessage with Piece
    And Client closes connection

