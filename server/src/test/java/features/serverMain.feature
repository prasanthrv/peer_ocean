Feature: Test Basic Server Networking Functionality

  Scenario: Test initServer() with bound port
    Given The ServerMain object intialized with bound port
    Then Exception is raised

  Scenario: Test initServer() with available port
    Given The ServerMain object intialized with free port
    Then Logger logs server as listening


  Scenario: Test an echo message from client to server
    Given The ServerMain object intialized with free port
    Then Server starts listening






