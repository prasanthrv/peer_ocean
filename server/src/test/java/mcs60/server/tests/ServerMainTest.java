package mcs60.server.tests;

import cucumber.api.java.en.*;
import mcs60.server.ServerMain;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.fail;


/**
 * Created by dare on 06/05/15.
 */
public class ServerMainTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static String rootPath = System.getProperty("user.dir");
    private static Path logPath =  Paths.get(rootPath, "log", "log.txt");
    private static String curMsgID;

    private ServerMain server;

    @Given("^The ServerMain object intialized with bound port$")
    public void the_ServerMain_object_intialized_with_bound_port() throws Throwable {
        server = new ServerMain(80);
    }

    @Then("^Exception is raised$")
    public void exception_is_raised() {
        try {
            server.initServer(1000, "Peer");
            //fail();
        } catch (Exception e) {
            assert(e.getMessage().equals("Cannot init socket"));
        }
    }

    @Given("^The ServerMain object intialized with free port$")
    public void the_ServerMain_object_intialized_with_free_port() throws Throwable {
        //assuming 23450 is free
        if (server != null)
            server.terminate();
        server = new ServerMain(23450);
    }

    @Then("^Logger logs server as listening$")
    public void logger_logs_server_as_listening() throws Throwable {
        server.initServer(500, "Peer");
        String text = new String(Files.readAllBytes(logPath),
                StandardCharsets.UTF_8);

        assert(text.contains("Waiting for connection."));
    }



    @Then("^Server starts listening$")
    public void server_starts_listening() throws Throwable {
        server.initServer(500, "Peer");
    }

}
