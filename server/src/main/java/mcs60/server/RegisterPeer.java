package mcs60.server;

import mcs60.common.protocol.PeerAddress;
import mcs60.common.protocol.messages.ACKMessage;
import mcs60.common.protocol.messages.RegisterPeerMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.UUID;

/**
 * Created by dare on 7/31/2015.
 */
public class RegisterPeer {

    protected static final Logger log = LogManager.getRootLogger();
    private PeerAddress peerServer;
    private UUID peerID;


    RegisterPeer(PeerAddress peerServer, UUID peerID) {
        this.peerServer = peerServer;
        this.peerID = peerID;
    }

    public boolean register() {

        if (peerServer == null) {
            log.error("Cannot find peer server info");
            return false;
        }

        SocketAddress addr = new InetSocketAddress(peerServer.getIP(), peerServer.getPort());
        Socket sock = new Socket();

        try {
            sock.connect(addr, 1000);

            RegisterPeerMessage msg = new RegisterPeerMessage(InetAddress.getLocalHost().getHostAddress(), 23450, peerID,
                    UUID.randomUUID());

            ObjectOutputStream objOutStream = new ObjectOutputStream(sock.getOutputStream());
            objOutStream.writeObject(msg);
            objOutStream.flush();

            ObjectInputStream objInStream = new ObjectInputStream(sock.getInputStream());
            ACKMessage ack = (ACKMessage) objInStream.readObject();

            if(ack.getMsgID() == msg.getMsgID()){
                return true;
            }
        } catch (SocketTimeoutException e) {
            log.error("Connection with PeerServer timed out");
            return false;
        } catch (IOException e) {
            log.error("Connection with PeerServer threw IOException");
            return false;
        } catch (ClassNotFoundException e) {
            log.error("Cannot cast object from PeerServer");
            return false;
        }

        return false;
    }




}
