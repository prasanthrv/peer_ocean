package mcs60.server;

import java.util.Date;

/**
 * Created by dare on 21/05/15.
 */
public class PieceInfo {
    private String hash;
    private String path;
    private Date date;
    private long size;

    public PieceInfo(String hash, String path, Date date, long size) {
        this.hash = hash;
        this.path = path;
        this.date = date;
        this.size = size;
    }

    public String getHash() {
        return hash;
    }

    public String getPath() {
        return path;
    }

    public Date getDate() {
        return date;
    }

    public long getSize() {
        return size;
    }
}
