package mcs60.server;

/**
 * Created by dare on 7/28/2015.
 * Class for representing a <key,value> for a row in the key_values table
 */
public class KeyValue {
    private String key;
    private String value;

    public KeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
