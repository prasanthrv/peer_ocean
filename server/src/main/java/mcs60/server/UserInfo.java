package mcs60.server;

/**
 * Created by dare on 20/05/15.
 */
public class UserInfo {

    private String name;
    private String pwd;
    private String client;

    public String getName() {
        return name;
    }

    public String getPwd() {
        return pwd;
    }

    public String getClient() {
        return client;
    }


    public UserInfo(String name, String pwd, String client) {
        this.name = name;
        this.pwd = pwd;
        this.client = client;
    }
}
