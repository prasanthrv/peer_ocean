package mcs60.server.configs;

import java.util.Arrays;

/**
 * Created by dare on 7/30/2015.
 */
public class RunApp {

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Wrong number of arguments");
            return;
        }

        String[] shifted_args = Arrays.copyOfRange(args, 1, args.length);

        switch (args[0]) {
            case "peer": {

                RunPeer.main(shifted_args);
                break;
            }

            case "peer_server": {

                RunPeerServer.main(shifted_args);
                break;
            }

            case "modify_default_port": {

                ModifyDefaultPort.main(shifted_args);
                break;
            }

            case "modify_server_addr": {

                ModifyPeerServer.main(shifted_args);
                break;
            }
        }
    }
}
