package mcs60.server.configs;


import mcs60.server.*;
import mcs60.common.protocol.PeerAddress;
import mcs60.server.UpdatePeerList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by dare on 7/30/2015.
 */
public class RunPeerServer {
    protected static final Logger log = LogManager.getRootLogger();

    public static void main(String[] args) {
        System.out.println("Running as a peer server:");

        String[] type  = {"PeerServer", "1000"};
        try {
            ServerMain.main(type);
        } catch (Exception e) {
            log.error("Cannot initialize peer server");

        }

    }
}
