package mcs60.server.configs;

import mcs60.server.ServerMain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by dare on 7/30/2015.
 */
public class RunPeer {

    protected static final Logger log = LogManager.getRootLogger();

    public static void main(String[] args) {
        System.out.println("Running as a peer");

        String[] type  = {"Peer", "0" };
        try {
            ServerMain.main(type);
        } catch (Exception e) {
            log.error("Cannot initialize peer server");

        }

    }
}
