package mcs60.server.configs;

import mcs60.server.DataManager;

import java.util.regex.Pattern;

/**
 * Created by dare on 7/28/2015.
 */
public class ModifyPeerServer {

    private static void printFormatError() {
        System.out.println("Wrong format, Correct format: <ip_address> <port>");
    }

    public static void main(String[] args) {
        boolean sanitised = false;
        if (args.length != 2) {
            sanitised = false;
            System.out.println("Wrong number of arguments");
            printFormatError();
            return;
        }

        final String serverIP = args[0];
        final String serverPort = args[1];

        final Pattern IPV4_PATTERN = Pattern.compile(
        "^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");

        /* is it valid IPv4 address ? */
        if(!IPV4_PATTERN.matcher(serverIP).matches()){
            printFormatError();
            return;
        }

        /* is port a number and within valid range ?  */
        try {
            int port = Integer.parseInt(serverPort);
            if (port <0 || port >65535)
                throw new Exception();
        } catch (Exception e) {
            printFormatError();
            return;
        }

        /* add to database */
        DataManager.getInstance().modifyPeerServerInfo(serverIP, serverPort);
        System.out.println("Successfully modified PeerServer info");


    }
}
