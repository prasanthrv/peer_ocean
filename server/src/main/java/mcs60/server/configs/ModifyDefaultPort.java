package mcs60.server.configs;

import mcs60.server.DataManager;

import java.util.regex.Pattern;

/**
 * Created by dare on 7/28/2015.
 */
public class ModifyDefaultPort {
    private static void printFormatError() {
        System.out.println("Wrong format, Correct format: <port>");
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Wrong number of arguments");
            printFormatError();
            return;
        }

        final String serverPort = args[0];


        /* is port a number and within valid range ?  */
        try {
            int port = Integer.parseInt(serverPort);
            if (port <0 || port >65535)
                throw new Exception();
        } catch (Exception e) {
            printFormatError();
            return;
        }

        /* add to database */
        DataManager.getInstance().modifyDefaultPort(serverPort);
        System.out.println("Successfully modified default port");


    }
}
