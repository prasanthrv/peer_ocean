package mcs60.server;

/**
 * Created by dare on 06/05/15.
 */
import mcs60.common.protocol.PeerAddress;
import org.apache.logging.log4j.*;

import java.io.*;
import java.net.*;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

public class ServerMain {

    /** Logger */
    private static final Logger log = LogManager.getRootLogger();

    private ServerSocket socket;
    private int port;
    private UUID serverID;
    private PeerAddress peerServer;

    List<PeerAddress> peers = new LinkedList<PeerAddress>();
    ReentrantLock lock = new ReentrantLock();

    public ServerMain(int port) {
        this.port = port;
    }

    private boolean initSocket(int timeout) {


        int retry = 0;
        socket = null;
        serverID = UUID.randomUUID();

        try {

            socket = new ServerSocket(port);
            if(timeout != 0)
                socket.setSoTimeout(timeout);

        } catch (IOException e) {

            /* close malformed socket */
            try {
                if (socket != null && !socket.isClosed()) {
                    socket.close();
                    socket = null;
                }
            } catch (Exception ek) {
                log.error("Cannot close socket on error");
            }

            /* If the socket cannot be bound */
            if(e instanceof BindException) {
                log.error("Cannot bind to address " + this.port) ;
            }

            return  false;
        }

        return true;

    }


    public void initServer(int timeout, String type) throws Exception {

        if (type.equals("Peer")) {

            peerServer = DataManager.getInstance().getPeerServerInfo();

            if (peerServer == null) {
                log.error("Cannot find PeerServer address");
                return;
            }

            RegisterPeer peer = new RegisterPeer(peerServer, serverID);
            if (!peer.register()) {
                log.error("Cannot register peer with PeerServer");
                return;
            };
        }
        else if (type.equals("PeerServer"))
            (new UpdatePeerList(peers, lock, 5000)).run();

        log.info("Initializing server.");

        if (!initSocket(timeout)) {
            throw new RuntimeException("Cannot init socket");
        }

        DataManager.getInstance();

        /* loop for requests */
        while (true) {
            try {

                log.trace("Waiting for connection.");
                Socket clientSocket = socket.accept();
                log.trace("Socket connected");
                /* create thread */
                if (type.equals("Peer"))
                    new Thread(new RequestRunner(clientSocket, serverID)).start();
                else
                    new Thread(new PeerServerRunner(clientSocket,peers, lock, serverID)).start();


            } catch (SocketTimeoutException e) {
                log.trace("Socket timed out.");
                socket.close();
                break;
            } catch (IOException e) {
                log.error("Cannot accept connection...");
                break;
            }
        }

        // Intialize database

    }

    public void terminate() throws Exception {
       if (socket != null && !socket.isClosed())
           socket.close();
    }



    public static void main(String[] args) throws  Exception{
        ServerMain server = new ServerMain(23450);
        if (args.length < 2)
            System.out.println("Error in arguments");

        server.initServer(Integer.parseInt(args[1]), args[0]);


    }

}
