package mcs60.server;

import mcs60.common.protocol.PeerAddress;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by dare on 7/31/2015.
 */
public class UpdatePeerList implements Runnable {
    List<PeerAddress> peerList;
    ReentrantLock lock;
    int timeout;

    public UpdatePeerList(List<PeerAddress> peerList, ReentrantLock lock, int timeout) {
        this.peerList = peerList;
        this.lock = lock;
        this.timeout = timeout;
    }

    private void updatePeers() {
        lock.lock();
        peerList = DataManager.getInstance().getPeerList();
        lock.unlock();
    }

    @Override
    public void run() {

        updatePeers();
        while (true) {
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                ;
            }
        }
    }
}
