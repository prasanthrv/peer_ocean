package mcs60.server;

import mcs60.common.protocol.PeerAddress;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.UUID;

/**
 * Created by dare on 7/28/2015.
 */
public class PeerCollector implements Runnable{

    private static final Logger log = LogManager.getRootLogger();

    private String peerServerIP;
    private int peerServerPort;
    private Boolean registered = false;

    private Socket client;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private UUID serverID;

    public PeerCollector() {

    }

    /* get peer server info from the database */
    private boolean populatePeerServerInfo() {
        PeerAddress peerServer = DataManager.getInstance().getPeerServerInfo();

        if (peerServer == null) {
            return false;
        } else {
            this.peerServerIP = peerServer.getIP();
            this.peerServerPort = peerServer.getPort();
        }

        return true;
    }

    private boolean connect() {

        return true;
    }

    @Override
    public void run() {

        if(!populatePeerServerInfo()) {
            log.error("No default peer server specified, exiting peer collector");
            return;
        }

        while (true) {
            ;
        }

    }
}
