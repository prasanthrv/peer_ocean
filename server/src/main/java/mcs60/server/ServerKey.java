package mcs60.server;

import mcs60.common.utility.Util;

import java.util.UUID;

/**
 * Created by dare on 18/05/15.
 */
public class ServerKey {

    private static ServerKey instance = null;
    private static boolean initialized;
    private String key;

    // create key (using SHA 512)
    private ServerKey() {
        key = Util.hashSHA512(UUID.randomUUID().toString().getBytes());
    }

    public static ServerKey getInstance() {
        if(!initialized) {
            instance = new ServerKey();
            return instance;
        }
        return  instance;
    }


    public String getKey() {
        return  key;
    }

}
