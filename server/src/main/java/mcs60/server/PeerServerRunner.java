package mcs60.server;

import mcs60.common.protocol.PeerAddress;
import mcs60.common.protocol.messages.MessageFrame;
import mcs60.common.protocol.messages.PeerListMessage;
import mcs60.common.protocol.messages.RegisterPeerMessage;

import java.net.Socket;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by dare on 7/31/2015.
 */
public class PeerServerRunner extends RequestRunner {
    List<PeerAddress> peerList;
    private ReentrantLock lock;

    public PeerServerRunner(Socket socket, List<PeerAddress> peers, ReentrantLock lock, UUID serverID) {
        super(socket, serverID);
        this.lock = lock;
        this.peerList = peers;
    }

    @Override
    protected void processRequest(MessageFrame msg) {

        switch (msg.getRequestType()) {
            case REG_PEER: {
                processRegisterMessage(msg);
                break;
            }

            case GET_PEERS: {
                log.trace("Got a GetPeersMessage");
                processGetPeersMessage(msg);
                break;
            }

            default:
                break;
        }
    }

    private void processRegisterMessage(MessageFrame msg) {
        log.trace("Got a peer register message");
        RegisterPeerMessage req = (RegisterPeerMessage) msg;
        PeerAddress addr = new PeerAddress(req.getIP(), req.getPort());
        sendACK(req.getMsgID());
        DataManager.getInstance().addNewPeer(addr);
    }

    private void processGetPeersMessage(MessageFrame msg) {
        log.trace("Got a GetPeersMessage");

        if (sendACK(msg.getMsgID())) {
            sendPeerListMessage();
        }


    }

    private void sendPeerListMessage() {
        lock.lock();
        sendMessage(new PeerListMessage(peerList, serverID));
        lock.unlock();
    }
}
