package mcs60.server;

import mcs60.common.bundle.Piece;
import mcs60.common.protocol.messages.*;
import mcs60.common.utility.Util;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.*;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;

/**
 * Created by dare on 06/05/15.
 */
public class RequestRunner implements Runnable {

    /** Logger */
    protected static final Logger log = LogManager.getRootLogger();
    private static final DataManager dataManager = DataManager.getInstance();

    private Socket client;
    private ObjectInputStream inObjStream;
    private ObjectOutputStream outObjStream;
    private InputStream inStream;
    private OutputStream outStream;

    private BufferedOutputStream bufOut;
    private BufferedInputStream bufIn;

    protected UUID serverID;
    private final int BUFFER_SIZE = Util.BUFFER_SIZE;

    final private int timeout;
    private boolean flagExit;
    private String clientKey;

    private Path piecePath = Paths.get(System.getProperty("user.dir"), "pieces");


    public RequestRunner(Socket socket, UUID serverID) {
        client = socket;
        this.serverID = serverID;
        flagExit = false; // triggers with ExitMessage
        timeout = 5 * 1000; // secs * 1000
    }

    /**
     * Start the thread for the request
     */
    public void run() {

        int countdown = 0;
        MessageFrame msg;

        log.trace("Thread started for socket");

        // set timeout on server for read operations
        try {
            client.setSoTimeout(timeout);
        } catch (SocketException e) {
            log.error("Could not set timeout");
            return;
        }

        /* initialize streams */
        try {
            //bufIn = new BufferedInputStream(client.getInputStream());
            //bufOut = new BufferedOutputStream(client.getOutputStream());
            inStream = client.getInputStream();
            outStream = client.getOutputStream();

            outObjStream = new ObjectOutputStream(outStream);
            inObjStream = new ObjectInputStream(inStream);

        } catch (IOException e) {
            log.error("Cannot initialize streams...");
            return;
        }

        while(true) {
            try {
            /* read message */

                //if(client.getInputStream().)

                /* if EXIT message had be received */
                if (flagExit)
                    break;

                //in = new ObjectInputStream(client.getInputStream());
                //in.reset();
                Object obj = inObjStream.readObject();
                msg = (MessageFrame) obj;
                processRequest(msg);
                countdown = 0;

            } catch (SocketTimeoutException e) {  // socket timed out
                log.error("Socket timed out");
                break;
            } catch (EOFException e) { // the stream has no more data
                if (countdown >= timeout)
                    break;

                try { // sleep the thread for 50 ms
                    countdown += 50;
                    Thread.sleep(50);
                } catch (InterruptedException ex) {
                    log.trace("Thread interrupted");
                }

                continue;
            } catch (IOException e) {

                e.printStackTrace();
                log.error("IO error occurred while trying to get input/output stream from socket");
                break;

            } catch (ClassNotFoundException e) {
                log.error("Cannot read MessageFrame");
            }
        }

        //System.out.println("Number of active threads from the given thread: " + Thread.activeCount());

        log.trace("Thread ended");

    }


    /**
     * Process current request.
     *
     * @param msg
     *          the message frame
     */
    protected void processRequest(MessageFrame msg) {

        switch (msg.getRequestType()) {

            case ECHO: {
                processEchoMessage((EchoMessage) msg);
                break;
            }

            case ACK: {
                processAckMessage((ACKMessage) msg);
                break;
            }

            case INFO: {
                processInfoMessage((InfoMessage) msg);
                break;
            }

            case PIECE_CONTENT: {
                processPieceContentMessage((PieceContentMessage) msg);
                break;
            }

            case PIECE_RQ: {
                processPieceRequestMessage((PieceRequestMessage) msg);
                break;
            }

            case CLOSE_CONN: {
                processExitMessage((ExitMessage) msg);
                break;
            }

            case REQ_KEY: {
                processRequestKeyMessage((RequestKeyMessage) msg);
                break;
            }

            case AUTH_INFO: {
                processAuthMessage((AuthMessage) msg);
                break;
            }

            default: {
                log.error("Request type not recognized." );
            }
        }
    }

    private void processEchoMessage(EchoMessage msg) {
        logTraceWithID("Got an ECHO message", msg.getStationID());
        sendACK(msg.getMsgID());
    }

    private void processAckMessage(ACKMessage msg) {
        logTraceWithID("Got an ACK message", msg.getStationID());
    }

    private void processInfoMessage(InfoMessage msg) {
        logTraceWithID("Got an INFO message", msg.getStationID());
        logTraceWithID("INFO: " + msg.getInfo(), msg.getStationID());
        sendACK(msg.getMsgID());
    }

    private void processPieceContentMessage(PieceContentMessage msg) {
        String hash = msg.getPieceInfo().getHashOne();
        logTraceWithID("Got a PIECE CONTENT message", msg.getStationID());
        logTraceWithID("Piece Hash: " + hash, msg.getStationID());
        sendACK(msg.getMsgID());

        String path = Paths.get(piecePath.toString(), hash).toString();
        if (dataManager.findPiece(hash) == null) {
            writePiece(path, msg.getData());
            if (!dataManager.insertPiece(hash, path, (long) msg.getData().length))
                log.error("Could not enter piece information into database");
        }
    }

    private void processPieceRequestMessage(PieceRequestMessage msg){
        Piece req = msg.getPieceInfo();
        logTraceWithID("Got a PIECE REQUEST message", msg.getStationID());
        logTraceWithID("Piece Hash: " + req.getHashOne(), msg.getStationID());
        log.trace("Sending acknowledgment");
        sendACK(msg.getMsgID());

        log.trace("Finding piece in db");
        PieceInfo pieceInfo = dataManager.findPiece(req.getHashOne());
        /*if (pieceInfo != null) {
            log.trace("Sending piece length");
            if (sendPieceLength((int)pieceInfo.getSize(), msg.getStationID()))
                log.trace("Sending piece ");
                sendPiece(pieceInfo.getPath(), req.getHashOne(), msg.getStationID());
        } //else {
            ;
            */
            try {
                byte[] data = FileUtils.readFileToByteArray(new File(pieceInfo.getPath()));
                outObjStream.writeObject(new PieceContentMessage(new Piece(0, pieceInfo.getHash(), ""), data, serverID) );
                outObjStream.flush();
            } catch (IOException e) {
                logTraceWithID("Cannot send empty piece content message", msg.getStationID());
            }
        //}
    }



    /* not used atm */
    private void processRequestKeyMessage(RequestKeyMessage msg) {
        logTraceWithID("Got a key request", msg.getStationID());
        sendACK(msg.getMsgID());

        sendInfo(ServerKey.getInstance().getKey(), msg.getStationID());

    }

    private void processAuthMessage(AuthMessage msg) {
        boolean valid = false;

        logTraceWithID("Received an AuthMessage", msg.getStationID());
        sendACK(msg.getMsgID());
        ArrayList<UserInfo> info = dataManager.getUserInfo();

        if(info == null) {
            log.error("Cannot retreive user details");
            return;
        }

        // compare with retreived records
        for (UserInfo user : info) {
            if (Util.hashSHA256(user.getName().getBytes()).equals(msg.getUsername()) &&
                    Util.hashSHA256(user.getPwd().getBytes()).equals(msg.getPassword())) {
                valid = true;
                break;
            }
        }

        // send reply to client
        if(valid)
            sendInfo("Valid", msg.getStationID());
        else
            sendInfo("Invalid", msg.getStationID());

    }

    /*private boolean requestClientKey(UUID clientID) {

        logTraceWithID("Requesting key from client", clientID);
        try {
            out.writeObject(new RequestKeyMessage(serverID));
            out.flush();
        } catch (IOException e) {
            log.trace("Could not request key from client");
        }

        ACKMessage ackRequest = (ACKMessage) listen(clientID);
        if (ackRequest == null) {
           logTraceWithID("ACK not received for key request message", clientID);
            return false;
        }

        InfoMessage keyMsg;
        try {
            keyMsg = (InfoMessage) listen(clientID);
        } catch (ClassCastException e) {
            logTraceWithID("Cannot convert to INFOMessage", clientID);
            return false;
        }

        clientKey = keyMsg.getInfo();
        return true;
    }*/

    /**
     * Listen for messages from the client
     * @param clientID the client being listened to
     * @return the Message
     */
    protected MessageFrame listen(UUID clientID) {
        try {
            return (MessageFrame) inObjStream.readObject();
        } catch (Exception e) {
           logTraceWithID("Error while listening to", clientID);
        }
        return null;
    }

    private void processExitMessage(ExitMessage msg){
        logTraceWithID("Got an EXIT message", msg.getStationID());
        sendACK(msg.getMsgID());
        flagExit = true;
    }

    /**
     * Send piece to client
     * @param hash hash and name of file
     * @return true if success
     */
    private boolean sendPiece(String path, String hash, UUID clientID) {

        //Path piece = Paths.get(path);
        File piece = new File(path);

        //if (!Files.exists(piece)) {
        if (!Util.validateFile(path)) {
            logTraceWithID("Cannot locate file: " + path, clientID);
            return false;
        }

        int pieceLength = (int) piece.length();
        //pieceLength -= 1;

        log.trace("Piece length:" + pieceLength);

        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = 0;

        try {
            //log.trace("Initializing buffered streams");
            BufferedInputStream fileIn = new BufferedInputStream(new FileInputStream(piece));
            //BufferedOutputStream bufOut = new BufferedOutputStream(client.getOutputStream());
            //OutputStream bufOut = client.getOutputStream();

            while(bytesRead!= pieceLength && fileIn.read(buffer) != -1) {
                bytesRead += buffer.length;
                //log.trace("Write bytes: " + buffer.length);

                outStream.write(buffer);
                outStream.flush();

                // something here could be bad
                int remainingSize = pieceLength - bytesRead;
                if (remainingSize == 0) {
                    break;
                } else if ( remainingSize > 0 && remainingSize < BUFFER_SIZE) {
                    buffer = new byte[remainingSize];
                }

                /*if (bytesRead >= pieceLength) {
                    break;
                }
                */
            }

            log.trace("Total bytes written: " + bytesRead);
            fileIn.close();

        } catch (IOException e) {
            log.error("Error in sending piece");
            //e.printStackTrace();
            return false;
        }


        /*
        byte[] data;
        try {
            data = Files.readAllBytes(piece);
        } catch (IOException e) {
            logTraceWithID("Cannot read from file : " + path, clientID);
            return false;
        }

        try {
            out.writeObject(new PieceContentMessage(new Piece(0, hash, ""), data, serverID));
        } catch (IOException e) {
            logTraceWithID("Cannot send piece to client: " + hash, clientID);
            return false;
        }
        */

        log.trace("Completed piece transfer");
        return true;

    }

    protected boolean sendMessage(MessageFrame msg) {
        try {
            outObjStream.writeObject(msg);
            outObjStream.flush();
        } catch (IOException e) {
            logTraceWithID("Cannot send MessageInfo", msg.getStationID());
            return false;
        }
        return true;
    }

    private void sendInfo(String data, UUID clientID) {

        InfoMessage info = new InfoMessage(data,serverID);

        if (!sendMessage(info))
            logTraceWithID("Cannot send info message with info" + data, clientID);
    }

    /**
     * Send acknowledgment for message back to client
     * @param prevID id of the message that needs to acknowledged
     * @return true if success
     */
    protected boolean sendACK(UUID prevID) {

        if(!sendMessage(new ACKMessage(prevID, serverID))) {
            log.error("Cannot send ACK message, id: " + prevID);
            return false;
        }
        return true;
    }

    private boolean sendPieceLength(int length, UUID clientID) {

        if(!sendMessage(new PieceLengthMessage(length, clientID))) {
            log.error("Cannot send PieceLengthMessage to" + clientID);
            return false;
        }
        return true;
    }

    private void logTraceWithID(String msg, UUID clientID ) {
        log.trace(msg + " with id :"  + clientID.toString());
    }

    /**
     * Write piece to a file
     * @param path path to the file
     * @param data data to be written
     * @return true if success
     */
    private boolean writePiece(String path, byte[] data) {

        try {
            Files.write(Paths.get(path), data, StandardOpenOption.CREATE);
        } catch (IOException e) {
            log.error("Cannot write piece to file " + path );
            return false;
        }
        log.trace("Wrote piece to file: " + path);
        return true;
    }
}
