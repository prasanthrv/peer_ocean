package mcs60.server;

/**
 * Created by dare on 20/05/15.
 */

import mcs60.common.protocol.PeerAddress;
import mcs60.common.utility.DataAdapter;
import org.apache.logging.log4j.*;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.Date;

public class DataManager extends DataAdapter {



    private static final DataManager self = new DataManager();
    private static final Logger log = LogManager.getRootLogger();

    private static final String peerServerKey = "PeerServer";
    private static final String defaultPortKey = "DefaultPort";

    private DataManager() {
        connect("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/server_data", "server_app", "server");
    }

    /**
     * Get the instance
     * @return singleton instance
     */
    public static DataManager getInstance() {
        return self;
    }



    /**
     * Get user login details from database
     * @return user login details
     */
    public ArrayList<UserInfo> getUserInfo() {


        query = "Select * from auth_info";
        ArrayList<UserInfo> info = new ArrayList<UserInfo>();

        try {
            stmt = conn.createStatement();
            result = stmt.executeQuery(query);

            /* iterate results and add to ArrayList */
            while (result.next()) {
                info.add(new UserInfo(result.getString("username"),
                        result.getString("pswd"), result.getString("client_access")));
            }


            result.close();
            stmt.close();
            return info;
        } catch (SQLException e) {
            log.error("Could not run query");
        }
        return null;

    }

    public synchronized PieceInfo findPiece(String hash) {

        try (Statement stmt = conn.createStatement()){
            query = "select * from piece_info where hash='" + hash + "';";
            result = stmt.executeQuery(query);

            if(result.next()) {
                PieceInfo pieceInfo = new PieceInfo(result.getString("hash"), result.getString("path"),
                        result.getDate("last_access"), result.getLong("size"));
                result.close();
                return pieceInfo;
            }
            result.close();

        } catch (SQLException e) {
            throw new RuntimeException("Cannot run sql query for finding piece");
        }

        return null;
    }




    public synchronized boolean insertPiece(String hash, String path, Long size) {


        // a record already exists
        //if(findPiece(hash) != null)
         //   return true;

        try {
            query = "insert into piece_info (hash, path, last_access, size) values(?, ?, ?, ?);";
            prepStmt = conn.prepareStatement(query);
            prepStmt.setString(1, hash);
            prepStmt.setString(2, path);

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            prepStmt.setDate(3, new Date(new java.util.Date().getTime()));


            prepStmt.setLong(4,size);
            prepStmt.executeUpdate();
            prepStmt.close();

        } catch (SQLException e) {
            log.error("Cannot insert into piece_info");
            return false;
        }

        return true;
    }

    /* get a <key,value> pair from the key_values table */
    public synchronized KeyValue getKeyValue(String key) {

    query = "select * from key_values where key=?;";
        try (PreparedStatement prepStmt = conn.prepareStatement(query)){
            prepStmt.setString(1, key);
            result = prepStmt.executeQuery();

            if(result.next()) {
                return new KeyValue(result.getString("Key"), result.getString("value"));
            }
            result.close();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot run sql query for finding piece");
        }

        return null;
    }

    /* get the ip address & port of the peer server */
    public synchronized PeerAddress getPeerServerInfo() {

        KeyValue row = getKeyValue(peerServerKey);
        if(row == null)
            return null;

        String[] values = row.getValue().split(":");
        log.trace("Got peer server values: " + values[0] + ":" + values[1]);

        return new PeerAddress(values[0], Integer.parseInt(values[1]));
    }

    /* get the default port */
    public synchronized Integer getDefaultPort() {

        KeyValue row = getKeyValue(defaultPortKey);
        if(row == null)
            return null;

        return Integer.valueOf(row.getValue());
    }

    /* insert/update a <key,value> row in key_values */
    public synchronized boolean modifyKeyValue(String key, String value) {
        if (getKeyValue(key) != null) {
            query = "update key_values set value=? where key=?;";
            try (PreparedStatement prepStmt = conn.prepareStatement(query)) {
                prepStmt.setString(1, value);
                prepStmt.setString(2, key);

                prepStmt.executeUpdate();
                prepStmt.close();
                return true;

            } catch (SQLException e) {
                log.error("Cannot update " + key + " in key_values");
            }

        } else {
            query = "insert into key_values(key,value) values(?,?);";
            try (PreparedStatement prepStmt = conn.prepareStatement(query)) {
                prepStmt.setString(1, key);
                prepStmt.setString(2, value);

                prepStmt.executeUpdate();
                prepStmt.close();
                return true;

            } catch (SQLException e) {
                log.error("Cannot insert " + key + " in key_values");
                return false;
            }
        }
        return true;
    }

    /* insert/update PeerServer info */
    public boolean modifyPeerServerInfo(String ipaddr, String port) {

        return modifyKeyValue(peerServerKey, ipaddr + ":" + port );
    }

    /* insert/update Default port info */
    public boolean modifyDefaultPort(String port) {

        return modifyKeyValue(defaultPortKey, port);
    }


    public List<PeerAddress> getPeerList() {
        query = "select * from peer_info;";
        LinkedList<PeerAddress> peerInfo = new LinkedList<PeerAddress>();

        try (Statement stmt = conn.createStatement()) {
            result = stmt.executeQuery(query);

            while(result.next()) {
                peerInfo.add(new PeerAddress(result.getString("ipaddr"),
                        result.getInt("port")));
            }

        } catch (SQLException e) {
            log.error("Cannot get peer info.");
        }
        return peerInfo;
    }

    public boolean addNewPeer(PeerAddress peer) {

        query = "insert into peer_info(ipaddr, port) values(?,?);";

        try (PreparedStatement prepStmt = conn.prepareStatement(query)){

            prepStmt.setString(1, peer.getIP());
            prepStmt.setInt(2, peer.getPort());

            prepStmt.executeUpdate();

        } catch (SQLException e) {
            log.error("Cannot add peer details to peer_info");
            return false;
        }
        return true;
    }


}
