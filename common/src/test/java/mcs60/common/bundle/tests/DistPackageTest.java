package mcs60.common.bundle.tests;

import java.io.*;
import java.nio.file.*;
import java.util.List;

import mcs60.common.bundle.*;
import mcs60.common.utility.Util;
import org.apache.commons.io.IOUtils;

import static org.junit.Assert.*;
import cucumber.api.java.en.*;

public class DistPackageTest {

	private static DistPackage dist;
	private static List<FileAttribute> files;
	private static String rootPath = System.getProperty("user.dir");
	private static String piecePath = Paths.get(rootPath, "testDir","pieces").toString();
	private static String sourcePath = Paths.get(rootPath, "testDir", "src").toString();
	private static String destFile = Paths.get(rootPath, "testDir", "test.dist").toString();
	private static String destDir = Paths.get(rootPath, "testDir", "dest").toString();
	private static String distJSON;

	
	@Given("^The DistPackage Generator$")
	public void the_DistPackage_Generator() throws Throwable {
		//since the generator is static
		//passes by default
	}

	@When("^generate\\(\\) is invoked with a path$")
	public void is_invoked_with_a_clean_directory() throws Throwable {

		dist = DistPackageGenerator.generate(sourcePath);
		assertNotNull(dist);
	}	

	@Then("^The files should be enumerated$")
	public static void the_files_should_be_enumerated() throws Throwable {
		files = dist.getFileInfo();
		assertNotNull(files);
		assertTrue(files.size() > 0);
	    
	}

	@Then("^The optimal piece size for each file is calculated$")
	public static void the_optimal_piece_size_for_each_file_is_calculated() throws Throwable {
		for(FileAttribute file: files){
			assertTrue(file.getPieceSize() > 0);
		}
	}

	@Then("^Hash for each piece is calculated$")
	public static void hash_for_each_piece_is_calculated() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		for(FileAttribute file: files) {
			List<Piece> pieces = file.getPieceInfo();
			assertNotNull(pieces);

			for(Piece piece : pieces) {
				assertNotNull(piece.getId());
				assertNotNull(piece.getHashOne());
				assertNotNull(piece.getHashTwo());
			}
		}
	}

	@When("^The DistPackagePieceFileGenerator\\.generate\\(\\) is called$")
	public void the_DistPackagePieceFileGenerator_generate_is_called() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		DistPackagePieceFileGenerator.generate(piecePath, dist);
	}

	@Then("^Each piece is copied to new file with name as hash$")
	public static void each_piece_is_copied_to_new_file() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		for(FileAttribute file: files) {
			for(Piece piece: file.getPieceInfo()) {
				assertTrue(Util.validateFile(Paths.get(piecePath, piece.getHashOne()).toString()));
			}
		}
		
		
	}


	@When("^The DistJSON\\.convertToJSON\\(\\) is called$")
	public void the_DistJSON_convertToJSON_is_called() throws Throwable {
		distJSON = DistJson.convertToJSON(dist);
	}

	@Then("^JSON data is produced$")
	public static void json_data_is_produced() throws Throwable {
	   assertNotNull(distJSON); 
	}

	@Given("^A JSON representation of a DistPackage object$")
	public void a_JSON_representation_of_a_DistPackage_object() throws Throwable {
		;
	}


	@Then("^DistPackageFileGenerator\\.generate\\(\\) is called$")
	public void distpackagefilegenerator_generate_is_called() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		DistPackageFileGenerator.generate(destFile, dist);
	}


	@Then("^A \\.dist file is generated$")
	public void a_dist_file_is_generated() throws Throwable {
	    assertTrue(Util.validateFile(destFile));
	}

	@Given("^A \\.dist file$")
	public void a_dist_file() throws Throwable {
		;
	}

	@When("^The convertFromJSON\\(\\) is called$")
	public void the_convertFromJSON_is_called() throws Throwable {
		dist = null;
		String jsonData = IOUtils.toString(new FileInputStream(destFile)); 
		dist = DistJson.convertFromJSON(jsonData);
	}

	@Then("^A DistPackage object is generated$")
	public void a_DistPackage_object_is_generated() throws Throwable {
		assertNotNull(dist);
	}

	@Given("^The DistPackage Object$")
	public void a_DistPackage_object() throws Throwable {
		;
	}

	@When("^The assembly function is invoked$")
	public void the_assembly_function_is_invoked() throws Throwable {
		DistPackageAssembler.assemble(dist, destDir, piecePath);
	}

	@Then("^Original files are regenerated$")
	public void original_files_are_regenerated() throws Throwable {
		files = dist.getFileInfo();
		
		for(FileAttribute file : files) {
			String newFile = Paths.get(destDir, file.getPath()).toString();
			assertTrue(Util.validateFile(newFile));
			assertTrue(new File(newFile).length() == file.getSizeInBytes());
		}

	}
}
