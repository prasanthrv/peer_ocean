package mcs60.common.bundle.tests;


import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Files;

import mcs60.common.bundle.PieceHash;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PieceHashTest {
	
	private static PieceHash Hasher;
	private static byte[] messageBytes;
	private static String hash;

	@Given("^A PieceHash object and file contents$")	
	public static void setUpHasher() throws Exception {
		String fileName = "HashTest.txt";
		String testFilePath = PieceHashTest.class.getClassLoader().getResource(fileName).getPath();
		File testFile = new File(testFilePath);
		
		if (!testFile.exists()) {
			System.out.println("Cannot find test file at " + testFilePath);
		}

		messageBytes = Files.readAllBytes(testFile.toPath());
		Hasher = new PieceHash(messageBytes);
		//Hasher = new PieceHash(message.getBytes("US-ASCII"));
	}

	@When("^The first hash is requested$")
	public static final void testByteToHashOne() {
		hash = Hasher.byteToHashOne();
		//System.out.println("Generated SHA-256 hash: " + hash);
	}


	@When("^The second hash is requested$")
	public static final void testByteToHashTwo() {
		hash = Hasher.byteToHashTwo();
	}

	@Then("^The hash produced is not empty$")
	public static final void hashNotEmpty() {
		assertNotNull(hash);
	}

}
