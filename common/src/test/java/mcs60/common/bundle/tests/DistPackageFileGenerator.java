/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle.tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import mcs60.common.bundle.DistJson;
import mcs60.common.bundle.DistPackage;
import org.apache.logging.log4j.*;

/**
 * The Class DistPackageFileGenerator.
 */
public final class DistPackageFileGenerator {
	
	/** Logger */
	private static final Logger log = LogManager.getRootLogger();
	
	/**
	 * Instantiates a new dist package file generator.
	 */
	private DistPackageFileGenerator() {}
	
	/**
	 * Generate a .dist file for the distribution package.
	 *
	 * @param filePath
	 *            the target file path
	 * @param dist
	 *            the dist package
	 * @return true, if successful
	 */
	public static boolean generate(String filePath, DistPackage dist) {
		
		String json_data = DistJson.convertToJSON(dist);
		
		try {
			Files.write(Paths.get(filePath), json_data.getBytes());
		} catch (IOException e) {
			log.error("Cannot write file {}", filePath);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	

}
