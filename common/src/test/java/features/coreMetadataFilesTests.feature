Feature: Tests for the core file handling code

  Scenario: Test the first hash code generated is not empty
    Given A PieceHash object and file contents
    When The first hash is requested
    Then The hash produced is not empty

  Scenario: Test the second hash code generated is not empty
    Given A PieceHash object and file contents
    When The second hash is requested
    Then The hash produced is not empty

  Scenario: Test the file processing functions
    Given The DistPackage Generator
    When generate() is invoked with a path
    Then The files should be enumerated
    And The optimal piece size for each file is calculated
    And Hash for each piece is calculated

    Given The DistPackage Object
    When The DistPackagePieceFileGenerator.generate() is called
    Then Each piece is copied to new file with name as hash

    Given The DistPackage Object
    When The DistJSON.convertToJSON() is called
    Then JSON data is produced

    Given A JSON representation of a DistPackage object
    Then DistPackageFileGenerator.generate() is called
    And A .dist file is generated

    Given A .dist file
    When The convertFromJSON() is called
    Then A DistPackage object is generated

    Given The DistPackage Object
    When The assembly function is invoked
    Then Original files are regenerated
