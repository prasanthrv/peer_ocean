package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 17/05/15.
 */
public class AuthMessage extends MessageFrame{

    private String username;
    private String password;

    public AuthMessage(String username, String password, UUID id) {
        super(MESSAGE_TYPE.AUTH_INFO, id);
        this.username = username;
        this.password = password;
        setMsgID(UUID.randomUUID());
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
