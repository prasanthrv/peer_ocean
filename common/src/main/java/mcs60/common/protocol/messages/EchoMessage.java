package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 08/05/15.
 */
public class EchoMessage extends MessageFrame {

    public EchoMessage(UUID stationID) {
        super(MESSAGE_TYPE.ECHO,stationID);
        setMsgID(UUID.randomUUID());
    }
}
