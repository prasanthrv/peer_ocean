package mcs60.common.protocol.messages;

import mcs60.common.bundle.Piece;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by dare on 06/05/15.
 */
public class MessageFrame implements Serializable {


    public enum MESSAGE_TYPE {
        ECHO,
        ACK,
        PIECE_LENGTH,
        PIECE_CONTENT,
        PIECE_RQ,
        INFO,
        CLOSE_CONN,
        INIT_CONN,
        REQ_KEY,
        AUTH_INFO,
        REG_PEER,
        GET_PEERS,
        PEER_LIST
    }

    private MESSAGE_TYPE type;
    private UUID msgID; // the UUID of the message, for ACK
    private UUID stationID; // the client/server sending the message

    public MessageFrame(MESSAGE_TYPE type, UUID stationID) {
        this.type = type;
        this.stationID = stationID;
    }

    public MESSAGE_TYPE getRequestType() {
        return type;
    }


    public UUID getMsgID() {
        return msgID;
    }

    public UUID getStationID() {
        return stationID;
    }

    protected void setMsgID(UUID msgID) {
        this.msgID = msgID;
    }


}
