package mcs60.common.protocol.messages;

import mcs60.common.bundle.Piece;

import java.util.UUID;

/**
 * Created by dare on 08/05/15.
 */
public class PieceContentMessage extends DataMessage implements IPieceMessage {


    private Piece pieceInfo;

    public PieceContentMessage (Piece pieceInfo, byte[] data, UUID stationID) {
        super(MESSAGE_TYPE.PIECE_CONTENT, stationID);
        this.pieceInfo = new Piece(pieceInfo);
        setData(data);
        setMsgID(UUID.randomUUID());
    }

    @Override
    public Piece getPieceInfo() {
        return pieceInfo;
    }
}
