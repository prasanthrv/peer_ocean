package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 7/31/2015.
 */
public class RegisterPeerMessage extends MessageFrame {

    private String ipAddr;
    private int port;

    public RegisterPeerMessage( String ipAddr, int port, UUID stationID, UUID msgID) {
        super(MESSAGE_TYPE.REG_PEER, stationID);
        setMsgID(msgID);
        this.ipAddr = ipAddr;
        this.port = port;
    }

    public String getIP() {
        return ipAddr;
    }

    public int getPort() {
        return port;
    }
}
