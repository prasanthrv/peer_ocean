package mcs60.common.protocol.messages;

import mcs60.common.bundle.Piece;

import java.util.UUID;

/**
 * Created by dare on 08/05/15.
 */
public class PieceRequestMessage extends MessageFrame implements IPieceMessage {

    private Piece pieceInfo;

    public PieceRequestMessage(Piece pieceInfo, UUID stationID) {
        super(MESSAGE_TYPE.PIECE_RQ, stationID);
        this.pieceInfo = new Piece(pieceInfo);
        setMsgID(UUID.randomUUID());
    }

    @Override
    public Piece getPieceInfo() {
        return pieceInfo;
    }
}
