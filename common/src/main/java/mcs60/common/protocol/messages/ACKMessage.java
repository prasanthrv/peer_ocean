package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 08/05/15.
 */
public class ACKMessage extends MessageFrame{

    public ACKMessage(UUID msgID, UUID stationID) {
        super(MESSAGE_TYPE.ACK, stationID);
        setMsgID(msgID);
    }
}
