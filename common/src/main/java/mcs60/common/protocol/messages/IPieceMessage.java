package mcs60.common.protocol.messages;

import mcs60.common.bundle.Piece;

/**
 * Created by dare on 08/05/15.
 */
public interface IPieceMessage {
    public Piece getPieceInfo();
}
