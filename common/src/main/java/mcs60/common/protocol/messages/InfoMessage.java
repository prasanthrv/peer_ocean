package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 08/05/15.
 */
public class InfoMessage  extends DataMessage {

    public InfoMessage(String message, UUID stationID) {
        super(MESSAGE_TYPE.INFO,stationID);
        setData(message.getBytes());
        setMsgID(UUID.randomUUID());
    }

    public String getInfo() {
        return new String(getData());
    }
}
