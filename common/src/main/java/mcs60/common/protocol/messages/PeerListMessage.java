package mcs60.common.protocol.messages;

import mcs60.common.protocol.PeerAddress;

import java.util.List;
import java.util.UUID;

/**
 * Created by dare on 7/31/2015.
 */
public class PeerListMessage  extends  MessageFrame{
    List<PeerAddress> peers;

    public PeerListMessage(List<PeerAddress> peers, UUID stationID) {
        super(MESSAGE_TYPE.PEER_LIST, stationID);
        this.peers = peers;
    }

    public List<PeerAddress> getPeers() {
        return peers;
    }
}
