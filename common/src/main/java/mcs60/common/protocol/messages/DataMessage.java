package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 08/05/15.
 */
public class DataMessage extends MessageFrame {

    private byte[] data;
    private int contentSize;

    public DataMessage(MESSAGE_TYPE type, UUID stationID) {
        super(type, stationID);
        setMsgID(UUID.randomUUID());
    }

    public int getContentSize() {
        return contentSize;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
        this.contentSize = data.length;
    }
}
