package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 7/28/2015.
 */
public class PieceLengthMessage extends DataMessage {
    public PieceLengthMessage(int length, UUID stationID) {
        super(MESSAGE_TYPE.PIECE_LENGTH, stationID);
        setData(String.valueOf(length).getBytes());
        setMsgID(stationID);
    }

    public int getLength() {
        return Integer.parseInt(new String(getData()));
    }
}
