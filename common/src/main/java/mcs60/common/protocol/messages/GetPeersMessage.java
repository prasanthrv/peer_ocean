package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 7/31/2015.
 */
public class GetPeersMessage extends MessageFrame {
    public GetPeersMessage(UUID stationID) {
        super(MESSAGE_TYPE.GET_PEERS, stationID);
    }
}
