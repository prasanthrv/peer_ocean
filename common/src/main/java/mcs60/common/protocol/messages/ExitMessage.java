package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 12/05/15.
 */
public class ExitMessage extends MessageFrame {

    public ExitMessage (UUID clientID) {
        super(MESSAGE_TYPE.CLOSE_CONN, clientID);
        setMsgID(UUID.randomUUID());
    }
}
