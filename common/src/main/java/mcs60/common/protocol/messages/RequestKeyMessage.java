package mcs60.common.protocol.messages;

import java.util.UUID;

/**
 * Created by dare on 17/05/15.
 */
public class RequestKeyMessage extends MessageFrame {

    public RequestKeyMessage(UUID stationID) {
        super(MESSAGE_TYPE.REQ_KEY, stationID);
        setMsgID(UUID.randomUUID());
    }
}
