package mcs60.common.protocol;

import java.io.Serializable;

/**
 * Created by dare on 7/28/2015.
 */
public class PeerAddress implements Serializable{
    private String ipaddr;
    private int port;

    public String getIP() {
        return ipaddr;
    }

    public int getPort() {
        return port;
    }

    public PeerAddress(String ipaddr, int port) {
        this.ipaddr = ipaddr;
        this.port = port;
    }
}
