/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle;

import org.apache.logging.log4j.*;

import com.google.gson.*;

/**
 * The Class DistToJson.
 */
public final class DistJson {
	
	
	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	/** Logger */
    private static final DummyLogger log = new DummyLogger();
	//Logger log = LogManager.getRootLogger();

	/**
	 * Makes the class static.
	 */
	private DistJson() {}
	
	/**
	 * Convert a DistPackage to JSON.
	 *
	 * @param dist
	 *            The Distribution Package to convert to JSON
	 * @return JSON data
	 */
	public static String convertToJSON(DistPackage dist) {
		
		log.trace("Converting to JSON...");
		
		String json_data;
	    json_data = gson.toJson(dist);
	    
	    return json_data;
	}
	
	/**
	 * Convert a JSON string to a DistPackage
	 *
	 * @param serialData
	 * 			JSON string
	 * @return The Distribution Package to convert to JSON
	 */
	public static DistPackage convertFromJSON(String serialData) {
		
		log.trace("Converting from JSON...");
		
		return gson.fromJson(serialData, DistPackage.class);
		
	}

}
