package mcs60.common.bundle;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.logging.log4j.*;

import mcs60.common.utility.Util;
import org.apache.logging.log4j.core.util.FileUtils;

public final class DistPackageAssembler {

	private static final DummyLogger log = new DummyLogger();
	//private static final Logger log = LogManager.getRootLogger();

	private DistPackageAssembler() {}
	
	/**
	 *  Assembles the original files to destination directory
	 * @param dist 
	 * 			The DistPackage object containing details
	 * @param destDir 
	 * 			The destination directory
	 * @param pieceRepository
	 * 			The directory containing all the pieces
	 * @return
	 * 			
	 * @throws IOException
	 */
	public static boolean assemble(DistPackage dist, String destDir, String pieceRepository) throws IOException{
		
		//validate directories
		if (!Util.validateDirectory(destDir)) {
			log.error("Directory {} does not exist.", destDir);
			return false;
			//throw new IOException("Directory " + destDir + " does not exist.");
		}

		if (!Util.validateDirectory(pieceRepository)) {
			log.error("Directory {} does not exist.", pieceRepository);
			return false;
			//throw new IOException("Directory " + pieceRepository + " does not exist.");
		}
		
		int numberOfPieces;
		String filePath;
		String piecePath;
		byte[] buffer;
		FileInputStream pieceStream;
		FileOutputStream fileStream;

		for(FileAttribute file: dist.getFileInfo()) {
			
			log.trace("Assembling file {} ", file.getPath());
			
			filePath =  Util.combinePath(destDir, file.getPath());
			//Paths.get(destDir,file.getPath()).toString();
			File newFile = new File(filePath);

			// generate new directories, if needed
			if (!newFile.getParentFile().exists())
				newFile.getParentFile().mkdirs();

			fileStream = new FileOutputStream(newFile);
			buffer = new byte[file.getPieceSize()];
			numberOfPieces = file.getNumPieces();


			for(Piece piece: file.getPieceInfo()) {
				
				piecePath = Util.combinePath(pieceRepository, piece.getHashOne());
				//Paths.get(pieceRepository, piece.getHashOne()).toString();
				
				// cannot find piece
				if(!Util.validateFile(piecePath)) {
					log.error("Cannot find piece {}", piece.getHashOne());
					log.trace("Deleting file {}", filePath);
					fileStream.close();
                    new File(filePath).delete();
					//Files.delete(Paths.get(filePath));
					return false;
				}
				
				// last piece size
				if(piece.getId() == numberOfPieces - 1) 
					buffer = new byte[(int) (file.getSizeInBytes() - (piece.getId() * file.getPieceSize()))];
				
				// copy piece data
				log.trace("Copying piece {} id: {}", piecePath, String.valueOf(piece.getId()));
				pieceStream = new FileInputStream(piecePath);
				pieceStream.read(buffer);
				fileStream.write(buffer);
				pieceStream.close();
				
			}
			log.trace("Assembling completed.");
			fileStream.close();
		}

		return true;
		
	}

}
