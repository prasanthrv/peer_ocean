/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle;

/**
 * The Interface IByteDualHash.
 */
public interface IByteDualHash {
	
	/**
	 * Generate hash for bytes with method #1
	 *
	 * @return the hash
	 */
	public String byteToHashOne();
	
	/**
	 * Generate hash for bytes with method #2
	 *
	 * @return the hash
	 */
	public String byteToHashTwo();

}
