/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle;

import java.util.List;

/** This represent the actual distributed file metainfo. 
 *  It contains different file attribute structures. 
 * @author dare
 *
 */

public class DistPackage {
	
	/** Total number of files */
	private int fileNum;
	
	/** List of file attributes */
	private List<FileAttribute> fileInfo;
	
	/**
	 * Instantiates a new dist package.
	 *
	 * @param files
	 *            the files
	 */
	public DistPackage(List<FileAttribute> files) {
		fileInfo = files;
		fileNum = files.size();
	}

	/**
	 * Gets the list of file attributes.
	 *
	 * @return the file info
	 */
	public List<FileAttribute> getFileInfo() {
		return fileInfo;
	}

	/**
	 * Gets the total number of files.
	 *
	 * @return the number of files.
	 */
	public int getFileNum() {
		return fileNum;
	}
}
