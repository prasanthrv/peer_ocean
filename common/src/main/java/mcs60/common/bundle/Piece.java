/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle;

import java.io.Serializable;

/**
 * A class representing a piece.
 */
public class Piece  implements Serializable {
	
	/** The piece id. */
	private int pieceId;
	
	/** Hash #1 */
	private String hashOne;
	
	/** Hash #2 */
	private String hashTwo;
	
	/**
	 * Instantiates a new piece.
	 *
	 * @param id
	 *            the id
	 * @param hashOne
	 *            hash #1
	 * @param hashTwo
	 *            hash #2
	 */
	public Piece(int id, String hashOne, String hashTwo) {
		this.pieceId = id;
		this.hashOne = hashOne;
		this.hashTwo = hashTwo;
	}

	/**
	 *
	 * copy constructor
	 * @param p
	 * 		the piece to copy
	 */
	public Piece(Piece p) {
		this.pieceId = p.pieceId;
		this.hashOne = p.hashOne;
		this.hashTwo = p.hashTwo;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return pieceId;
	}

	/**
	 * Gets hash #1
	 *
	 * @return hash
	 */
	public String getHashOne() {
		return hashOne;
	}

	/**
	 * Gets hash #2
	 *
	 * @return hash
	 */
	public String getHashTwo() {
		return hashTwo;
	}
	
}
