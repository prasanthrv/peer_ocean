/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

import mcs60.common.utility.Util;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The Class DistPackageGenerator.
 * The class is used to generate a distribution package object.
 */
public final class  DistPackageGenerator {
	
	/** Logger */
	private static final DummyLogger log = new DummyLogger();
	//private static final Logger log = LogManager.getRootLogger();
	
	/** The source path. */
	private static String sourcePath;
	
	
	/**
	 * Instantiates a new dist package generator.
	 */
	private DistPackageGenerator() {}
	
	/**
	 * Generates a DistPackage object from a source directory.
	 *
	 * @param srcPath
	 *            the source directory
	 * @return a DistPackage object
	 * @throws IOException
	 *             in case of file errors
	 */
	public static DistPackage generate(String srcPath) throws IOException {
		sourcePath = srcPath;
		List<File> files;	
		List<FileAttribute> fileAtrributes = new LinkedList<FileAttribute>();
		
		log.trace("Validating path {}", sourcePath);
		Util.validatePath(sourcePath);
		
	    log.trace("Getting list of files in path...");
		files = Util.getFilesInPath(sourcePath);
		for(File file :files) {
			log.trace(file.toString());
		}

		FileAttribute fileInfo;
		//create a file attribute for each file
		log.trace("Generating FileAtrribute stuctures...");
		for(File file: files) {

			//skip symlinks
			//if (Files.isSymbolicLink(file.toPath()))
            if (FileUtils.isSymlink(file))
				continue;

			fileInfo = new FileAttribute(file, srcPath);
			fileInfo.generatePieceInfo();
			fileAtrributes.add(fileInfo);
		}
		
		return new DistPackage(fileAtrributes);
				
		
	}
	
		

}
