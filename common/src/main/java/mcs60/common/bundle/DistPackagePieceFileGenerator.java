/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mcs60.common.utility.Util;

/**
 * The Class DistPackagePieceFileGenerator.
 * Creates files for each piece in the distribution package
 * object. The names are same as the hash.
 */
public final class DistPackagePieceFileGenerator {
	
	/** The Constant log. */
    private static final DummyLogger log = new DummyLogger();
	//private static final Logger log = LogManager.getRootLogger();
	
	/**
	 * Instantiates a new dist package piece file generator.
	 */
	private DistPackagePieceFileGenerator() {} 

	/**
	 * Generate piece files
	 *
	 * @param path
	 *            the path
	 * @param dist
	 *            the dist object
	 * @return true, if successful
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static boolean generate(String path, DistPackage dist) throws IOException{
		
		if (!Util.validateDirectory(path)) {
			log.error("Given path is not a valid directory {}", path);
			throw new IOException("Cannot find directory...");
		}
		
		List<FileAttribute> fileInfo = dist.getFileInfo();
		
		return generateFiles(fileInfo, path);
		

	}
	
	
	/**
	 * Generate actual files.
	 *
	 * @param fileInfo
	 *            a list of file attributes
	 * @param destPath
	 *            the destination path
	 * @return true, if successful
	 */
	private static boolean generateFiles(List<FileAttribute> fileInfo,
			String destPath) {

		log.trace("Generating files.....");

		String flPath;
		String hash;
		String newPath = null;
		long fileSize;
		long startByte;
		long endByte;
		int pieceSize;
		int pieceID;

		for (FileAttribute file : fileInfo) {

			// get properties of current file
			flPath = file.getAbsolutePath();
			pieceSize = file.getPieceSize();
			fileSize = file.getSizeInBytes();
			byte[] bytes = new byte[pieceSize];

			try {
				log.trace("Processing file {}", flPath);
				FileInputStream readStream = new FileInputStream(flPath);

				for (Piece piece : file.getPieceInfo()) {

					// calculate start and end bytes
					pieceID = piece.getId();
					startByte = (pieceID * pieceSize);
					endByte = startByte + pieceSize;
					hash = piece.getHashOne();

					if (endByte > fileSize) {
						bytes = new byte[(int) (fileSize - startByte)];
					}

					try {

						// read bytes from original file
						readStream.read(bytes);

						// write bytes to new file
						newPath = Util.combinePath(destPath, hash);
						//newPath = Paths.get(destPath, hash).toString();
						FileOutputStream writeStream = new FileOutputStream(newPath);
						log.trace("Writing piece #{} to {}", String.valueOf(pieceID), newPath);
						writeStream.write(bytes);
						writeStream.close();

					} catch(IOException e) {

						log.error("Cannot write file {} from file {} ", newPath,
								flPath);
						return false;
					}

				}
				readStream.close();
			} catch(FileNotFoundException e) {
				log.error("Cannot open file {} for reading", flPath);
				return false;

			} catch(IOException e) {
				log.error("Cannot close file {}", flPath);
				return false;
			}
		}

		return true;

	}
	
	

}
