package mcs60.common.bundle;

import org.apache.logging.log4j.Logger;

/**
 * Created by dare on 03/07/15.
 */
public class DummyLogger {


    public DummyLogger () {
    }

    public void trace(String str, String str2, String str3, String str4) {
    }

    public void trace(String str, String str2, String str3) {
    }

    public void trace(String str, String str2) {
    }

    public void trace(String str) {
    }

    public void error(String str, String str2, String str3) {
    }

    public void error(String str, String str2) {
    }

    public void error(String str) {
    }


}
