/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mcs60.common.utility.Util;

/**
 * The Class FileAttribute.
 */
public class FileAttribute {
	
	/** Logger */
    private static final DummyLogger log = new DummyLogger();
	//private static final Logger log = LogManager.getRootLogger();
	
	/** File object for current file */
	private transient File file;
	
	/** The absolute path. */
	private transient String absolutePath;
	
	/** Relative path to base directory */
	private String path;
	
	/** Name of file */
	private String name;
	
	/** Size of each piece */
	private int pieceSize;
	
	/** Total number of pieces */
	private int numPieces;
	
	/** Size of file */
	private long size;
	
	/** List of pieces */
	private List<Piece> pieces;
	
	/**
	 * Instantiates a new file attribute.
	 *
	 * @param fl
	 *            the source file as input
	 * @param baseDir
	 *            the base directory
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public FileAttribute(File fl, String baseDir) throws IOException {
		
		file = fl;

		// check file validity
		if(!Util.validateFile(file.getPath())) {
			log.error("Cannot find file {}", file.getPath());
			throw new IOException("Cannot find file...");
		}
		
		// get relative path
		//Path filePath = Paths.get(file.getAbsolutePath());
		//Path basePath = Paths.get(baseDir);
		//path = basePath.relativize(filePath).toString();

		String filePath = file.getAbsolutePath();
		path = new File(baseDir).toURI().relativize(new File(filePath).toURI()).getPath();

		absolutePath = file.getAbsolutePath();
		name = file.getName();
		size = file.length();
		
		log.trace("Created FileAttribute with path:{}, lenght:{}", path,String.valueOf(size));
		
	}
	
	/**
	 * Populates piece list.
	 */
	public void generatePieceInfo() {

		generatePieceLength();
		log.trace("Generated PieceSize: {}", String.valueOf(pieceSize));
		log.trace("Number of pieces: {}", String.valueOf(numPieces));

		byte[] data = new byte[pieceSize];
		pieces = new LinkedList<Piece>();
		
		String hashOne, hashTwo;

		try {
			FileInputStream stream = new FileInputStream(file);

			int id = 0;
			while (stream.available() > 0) {

				// last piece
				if (stream.available() <= pieceSize) {
					log.trace("Last piece size:{}", String.valueOf(stream.available()));
					data = new byte[stream.available()];
				}

				stream.read(data);
				PieceHash hasher = new PieceHash(data);
				hashOne = hasher.byteToHashOne();
				hashTwo = hasher.byteToHashTwo();
				pieces.add(new Piece(id++,hashOne, hashTwo));
				log.trace("ID: {} Hash1: {} Hash2: {}", String.valueOf(id -1), String.valueOf(hashOne), String.valueOf(hashTwo));
			}
			stream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Generate optimal piece length.
	 */
	private void generatePieceLength() {

		long fileSize = file.length();
		pieceSize = 1024;

		// if file size is less than 1 MB
		if (fileSize < 1024 * 1024) { 
			pieceSize = (int) file.length();
			return ; 
		}

        // if file is more than 1 MB
        int tmpSize = 256 * 1024; //256 KB
        int newSize;
        long newNumPieces;
        while ( true ) {
            newNumPieces = fileSize / (tmpSize*2) ;

            if( tmpSize < 1024 * 1024 && newNumPieces <256 && newNumPieces > 4 ) // needs adjustment
                tmpSize = tmpSize * 2;
            else
                break;
            //System.out.println(String.valueOf(tmpSize));
        }

        pieceSize = tmpSize;
		

		//int optPiece = (int) fileSize / 255;
		//pieceSize = optPiece;

		//otherwise get max piece size for less than 
		// 256 pieces
		//while(fileSize/pieceSize > 256) {
		//	pieceSize = pieceSize + (pieceSize / 2); 
		//}
		
		numPieces = ((int) (fileSize / pieceSize)) + 1;
		
	} 
	
	/**
	 * Gets the absolute path.
	 *
	 * @return the absolute path
	 */
	public String getAbsolutePath() {
		return absolutePath;
	}

	/**
	 * Gets the size in bytes.
	 *
	 * @return the size in bytes
	 */
	public long getSizeInBytes() {
		return size;
	}

	/**
	 * Gets file name
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the relative path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * Gets the pieces.
	 *
	 * @return the list of pieces.
	 */
	public List<Piece> getPieceInfo() {
		return pieces;
	}

	/**
	 * Gets the piece size.
	 *
	 * @return the piece size
	 */
	public int getPieceSize() {
		return pieceSize;
	}

	/**
	 * Gets the number of  pieces.
	 *
	 * @return the number of pieces
	 */
	public int getNumPieces() {
		return numPieces;
	}

}
