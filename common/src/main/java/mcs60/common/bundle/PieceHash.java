/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.bundle;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import mcs60.common.utility.Util;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * The Class to generate hashes for a piece.
 */
public class PieceHash implements IByteDualHash {
	

	/** Message in bytes */
	private byte[] message;
	
	/**
	 * Instantiates a new piece hash.
	 *
	 * @param msg
	 *            the message in bytes
	 * @throws NoSuchAlgorithmException
	 *             if the hashing algorithm is not found
	 */
	public PieceHash(byte[] msg) throws NoSuchAlgorithmException  {
		message = msg;
	}

	/**
	 * Hash using SHA 256.
	 *
	 * @throws NoSuchAlgorithmException
	 *             if the hashing algorithm is not found
	 * @return hash string
	 */
	public String byteToHashOne() {
		return Util.hashSHA256(message);
	}

	/**
	 * Hash using MD5.
	 *
	 * @throws NoSuchAlgorithmException
	 *             if the hashing algorithm is not found
	 * @return hash string
	 */
	public String byteToHashTwo() {
		return Util.hashMD5(message);
		}
	
	/* code for calculating MD5 hash corresponding to PHP etc
	 * 	
		String input = new String(message);
		String result = input;
	    MessageDigest md = null;
	    if(input != null) {
			try {
				md = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //or "SHA-1"
	        md.update(input.getBytes());
	        BigInteger hash = new BigInteger(1, md.digest());
	        result = hash.toString(16);
	        while(result.length() < 32) { //40 for SHA-1
	            result = "0" + result;
	        }
	    }
	    return result;	

	 */

}
