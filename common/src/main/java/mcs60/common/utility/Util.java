/*
 * Copyright 2015- Prasanth Ravi
 */
package mcs60.common.utility;

import java.io.*;
import java.nio.file.*;
import java.security.MessageDigest;
import java.util.*;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

/**
 * The Class Util.
 * A utility class with static methods for 
 * common usage.
 */
public final class Util {

    private final static MessageDigest sha512 = DigestUtils.getSha512Digest();
    private final static MessageDigest sha256 = DigestUtils.getSha256Digest();
    private final static MessageDigest md5 = DigestUtils.getMd5Digest();

	public final static int BUFFER_SIZE = 4096;
	/**
	 * Instantiates a new util.
	 */
	private Util() {}

    /**
     * Hash data with SHA-512
     * @param data the data to be hashed
     * @return hash as string
     */
	public static String hashSHA512(byte[] data) {
        return new String(Hex.encodeHex(DigestUtils.sha512(data)));
        //return DigestUtils.sha512Hex(sha512.digest(data));
    }

    /**
     * Hash data with SHA-256
     * @param data the data to be hashed
     * @return hash as string
     */
    public static String hashSHA256(byte[] data) {
        return new String(Hex.encodeHex(DigestUtils.sha256(data)));
        //return DigestUtils.sha256Hex(sha256.digest(data));
    }

    /**
     * Hash data with MD5
     * @param data the data to be hashed
     * @return hash as string
     */
    public static String hashMD5(byte[] data) {
        return new String(Hex.encodeHex(DigestUtils.md5(data)));
        //return DigestUtils.md5Hex(md5.digest(data));
    }

	/**
	 * Validate path.
	 *
	 * @param path
	 *            the path
	 * @return true, if successful
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static boolean validatePath(String path) throws IOException {
		
		if(!new File(path).exists())
			return true;

		return false;
	}
	
	/**
	 * Validate directory.
	 *
	 * @param path
	 *            the path
	 * @return true, if successful
	 */
	public static boolean validateDirectory(String path) {
		
		//if(Files.isDirectory(Paths.get(path)))
        if(new File(path).isDirectory())
			return true;
		
		return false;
		
	}
	
	/**
	 * Validate file.
	 *
	 * @param path
	 *            the path
	 * @return true, if successful
	 */
	public static boolean validateFile(String path) {
		
		//if(Files.isRegularFile(Paths.get(path))) {
        if (new File(path).isFile()) {
			return true;
			
		}
		
		return false;
	}

	/**
	 * Gets the files in path.
	 *
	 * @param path
	 *            the path
	 * @return the files in path
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static List<File> getFilesInPath(String path) throws IOException {
	
		
		File filePath = new File(path);

		return new LinkedList<File>(FileUtils.listFiles(filePath,null,true));
		

//		// enumerate file contents
//		if(filePath.isFile()) {
//			files.add(filePath);
//		} else {
//			files = Files.walk(Paths.get(path))
//					.filter(Files::isRegularFile).map(Path::toFile)
//					.collect(Collectors.toCollection(LinkedList::new));
//		}
//		
//		return files;
	
	}

    /* Combine two paths using native delimiters */
	public static String combinePath(String dir, String file) {
        return FileUtils.getFile(dir,file).toString();
    }

    /* Recursively delete a directory */
	public static void deleteDir(String path) {
		File flTmpDir = new File(path);

		if(flTmpDir.isDirectory()) {
			for (File fl : flTmpDir.listFiles()) {
				if (fl.isDirectory()) { deleteDir(fl.getPath()); }
				fl.delete();
			}
		}
	}

}
