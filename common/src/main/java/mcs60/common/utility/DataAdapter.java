package mcs60.common.utility;


import java.sql.*;

/**
 * Created by dare on 20/05/15.
 */
public class DataAdapter {



    protected Connection conn;
    protected ResultSet result;
    protected Statement stmt;
    protected PreparedStatement prepStmt;
    protected String query;
    protected boolean isConnected = false;


    protected synchronized void connect(String driver, String connectionString, String username, String password) {
        if(isConnected)
            return;

        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(connectionString, username, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not find driver for " + driver);
        } catch (SQLException e) {
            System.out.printf(e.getMessage());
            e.printStackTrace();
        }

        isConnected = true;
    }

    public synchronized void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException("Cannot close database connection");
        }
    }

}
